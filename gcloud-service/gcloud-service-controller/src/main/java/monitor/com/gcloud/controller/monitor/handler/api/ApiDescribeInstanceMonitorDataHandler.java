package com.gcloud.controller.monitor.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.monitor.service.IMonitorService;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerMsg;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule = SubModule.VM ,action = "DescribeInstanceMonitorData", name = "云服务器监控数据")
public class ApiDescribeInstanceMonitorDataHandler extends MessageHandler<ApiDescribeInstanceMonitorDataHandlerMsg, ApiDescribeInstanceMonitorDataHandlerReplyMsg>{
	
	@Autowired
    private IMonitorService monitorService;

    @Override
    public ApiDescribeInstanceMonitorDataHandlerReplyMsg handle(ApiDescribeInstanceMonitorDataHandlerMsg msg) {
    	ApiDescribeInstanceMonitorDataHandlerReplyMsg reply = new ApiDescribeInstanceMonitorDataHandlerReplyMsg();
    	try {
			reply = monitorService.describeInstanceMonitorData(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return reply;
    }
}