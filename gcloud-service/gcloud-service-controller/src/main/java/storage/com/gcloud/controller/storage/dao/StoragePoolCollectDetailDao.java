package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.StoragePoolCollectDetail;
import com.gcloud.controller.storage.enums.CollectStorageInfoState;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
public class StoragePoolCollectDetailDao extends JdbcBaseDaoImpl<StoragePoolCollectDetail, String>{
	public <E> E statistic(String batchId, Class<E> clazz){
		StringBuffer sql = new StringBuffer();
		sql.append("select count(id) as poolNum, COALESCE(sum(avail),0) as poolAvailSize,COALESCE(sum(used),0) as poolUsedSize,COALESCE(sum(total),0) as poolTotalSize ");
		sql.append("from gc_storage_pool_collect_details ");
		sql.append("where batch_id=? ");
		sql.append("and EXISTS(SELECT p.id from gc_storage_pools p where p.id=pool_id)");//不固定batch_id后这句可以不�?
		
		List<Object> values = new ArrayList<>();
		values.add(batchId);
		
		List<E> ress = findBySql(sql.toString(), values, clazz);
		return ress.size()>0?ress.get(0):null;
	}
	
	public void deleteHistoryBatchData() {
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete d.* from gc_storage_pool_collect_details d where EXISTS(select b.batch_id from gc_storage_pool_collect_cron_tasks b where b.state=? and b.batch_id=d.batch_id)");
		values.add(CollectStorageInfoState.COLLECTED.value());
		
		this.jdbcTemplate.update(sql.toString(), values.toArray());
	}
}