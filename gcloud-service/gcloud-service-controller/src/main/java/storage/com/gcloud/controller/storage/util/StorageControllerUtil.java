package com.gcloud.controller.storage.util;

import java.text.MessageFormat;

import com.gcloud.service.common.Consts;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StorageControllerUtil {
	public static String getNodesAliveKey(String hostName) {
		return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODE_ALIVE, hostName);
	}

	public static String getNodesHostNameLock(String hostName) {
		return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODE_LOCK, hostName);
	}
	
	public static String getNodesHostNameKey(String hostName) {
		return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODES_STORAGE_NODE_HOSTNAME, hostName);
	}
	
	/**
     * @Title: getHostNameByAliveKey
     * @Description: 根据key来截取hostname，就不用在去获取�?次redis
     * @param aliveKey
     * @return hostname
     */
    public static String getHostNameByAliveKey(String aliveKey) {
        // 如果Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODE_ALIVE结构修改，需要跟�?修改
        int idx = Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODE_ALIVE.indexOf("{0}");
        String hostname = aliveKey.substring(idx);
        return hostname;

    }
    
    public static String getHostNameByHostKey(String hostKey) {

		// 如果Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODES_STORAGE_NODE_HOSTNAME结构修改，需要跟�?修改
		int idx = Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODES_STORAGE_NODE_HOSTNAME.indexOf("{0}");
		String hostname = hostKey.substring(idx);
		return hostname;

	}
    
    public static String getSnapLock(String volumeId, String snapshotId) {
		return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_SNAP_LOCK, volumeId, snapshotId);
	}
    
    public static String getPoolInfoCollectCronLock(String batchId) {
		return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_POOL_INFO_COLLECT_CRON_LOCK, batchId);
	}

}