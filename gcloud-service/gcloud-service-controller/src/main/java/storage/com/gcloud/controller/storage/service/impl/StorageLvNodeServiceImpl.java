package com.gcloud.controller.storage.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.storage.dao.StorageLvNodeDao;
import com.gcloud.controller.storage.entity.StorageLvNode;
import com.gcloud.controller.storage.service.IStorageLvNodeService;
import com.gcloud.header.storage.msg.node.volume.lvm.LvReportMsg;
import com.gcloud.header.storage.msg.node.volume.lvm.model.LvInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class StorageLvNodeServiceImpl implements IStorageLvNodeService {
	@Autowired
	StorageLvNodeDao storageLvNodeDao;

	@Override
	public void lvReport(LvReportMsg msg) {
		List<StorageLvNode> lvsInDb = storageLvNodeDao.findByProperty("hostname", msg.getHostname());
		Map<String, StorageLvNode> lvsMapInDb = lvsInDb.stream().collect(Collectors.toMap(StorageLvNode::getLvPath, Function.identity()));
		Map<String, LvInfo> lvsMapInReport = msg.getLvs().stream().collect(Collectors.toMap(LvInfo::getPath, Function.identity()));
		
		//汇报有，数据库中没有，需要新�?
		for(LvInfo lv:msg.getLvs()) {
			if(!lvsMapInDb.containsKey(lv.getPath())) {
				//add
				StorageLvNode addLvNode = new StorageLvNode();
				addLvNode.setHostname(msg.getHostname());
				addLvNode.setLvPath(lv.getPath());
				addLvNode.setState(lv.getStatus().name());
				
				storageLvNodeDao.save(addLvNode);
			}
		}
		
		//汇报没有，数据库中有，需要删�?
		for(StorageLvNode lvNode:lvsInDb) {
			if(!lvsMapInReport.containsKey(lvNode.getLvPath())) {
				//delete
				storageLvNodeDao.delete(lvNode);
			}
		}
	}

	@Override
	public List<StorageLvNode> findByPath(String lvPath) {
		return storageLvNodeDao.findByProperty("lvPath", lvPath);
	}

	@Override
	public List<StorageLvNode> findByImageId(String imageId) {
		return storageLvNodeDao.findByImageId(imageId);
	}
	
}