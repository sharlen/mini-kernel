package com.gcloud.controller.storage.handler.node.snapshot;

import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.storage.msg.node.volume.NodeDeleteSnapshotReplyMsg;

import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class NodeDeleteSnapshotReplyHandler extends AsyncMessageHandler<NodeDeleteSnapshotReplyMsg> {

    @Autowired
    private ISnapshotService snapshotService;

    @Override
    public void handle(NodeDeleteSnapshotReplyMsg msg) {
        if (msg.getSuccess()) {
            this.snapshotService.handleDeleteSnapshotSuccess(msg.getSnapshotId());
        }
        else {
            this.snapshotService.handleDeleteSnapshotFailed(msg.getErrorCode(), msg.getSnapshotId());
        }
    }

}