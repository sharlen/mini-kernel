package com.gcloud.controller.storage.handler.node.node;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.service.IStorageNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.storage.msg.node.node.StorageNodeConnectMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Handler
public class StorageNodeConnectHandler  extends AsyncMessageHandler<StorageNodeConnectMsg> {
	@Autowired
    private IStorageNodeService storageNodeService;
	@Override
	public void handle(StorageNodeConnectMsg msg) {
		storageNodeService.storageNodeConnect(msg.getHostname(), msg.getNodeTimeout());
	}

}