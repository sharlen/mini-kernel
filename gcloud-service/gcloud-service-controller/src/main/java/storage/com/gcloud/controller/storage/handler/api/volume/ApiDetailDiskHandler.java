package com.gcloud.controller.storage.handler.api.volume;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.DetailDiskParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.DetailDisk;
import com.gcloud.header.storage.msg.api.volume.ApiDetailDiskMsg;
import com.gcloud.header.storage.msg.api.volume.ApiDetailDiskReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DetailDisk",name="磁盘详情")
public class ApiDetailDiskHandler extends MessageHandler<ApiDetailDiskMsg, ApiDetailDiskReplyMsg>{

	@Autowired
    private IVolumeService volumeService;
	
	@Override
	public ApiDetailDiskReplyMsg handle(ApiDetailDiskMsg msg) throws GCloudException {
		DetailDiskParams params = BeanUtil.copyBean(msg, DetailDiskParams.class);
		DetailDisk response = volumeService.detailDisk(params, msg.getCurrentUser());
		
		ApiDetailDiskReplyMsg reply = new ApiDetailDiskReplyMsg();
		reply.setDetailDisk(response);
		return reply;
	}

}