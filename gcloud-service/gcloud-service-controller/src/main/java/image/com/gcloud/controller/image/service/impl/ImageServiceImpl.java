package com.gcloud.controller.image.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.model.DescribeImageParams;
import com.gcloud.controller.image.model.DetailImageParams;
import com.gcloud.controller.image.model.UploadImageParams;
import com.gcloud.controller.image.provider.IImageProvider;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.image.enums.ImageArchitecture;
import com.gcloud.header.image.enums.ImageStatus;
import com.gcloud.header.image.enums.OsType;
import com.gcloud.header.image.model.DetailImage;
import com.gcloud.header.image.model.ImageStatisticsItem;
import com.gcloud.header.image.model.ImageType;
import com.gcloud.header.image.msg.api.GenDownloadVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ImageServiceImpl implements IImageService {

    @Autowired
    private ImagePropertyDao imagePropertyDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private InstanceDao instanceDao;
    
    @Override
    public String createImage(CreateImageParams params, CurrentUser currentUser) throws GCloudException {

        if(StringUtils.isNotBlank(params.getImageId())){
            Image image = imageDao.getById(params.getImageId());
            if(image == null){
                throw new GCloudException("0090108::éåä¸å­å?");
            }

            Map<String, String> imageProps = getImageProperties(params.getImageId());
            if(imageProps != null){
                params.setOsType(imageProps.get(ImagePropertyItem.OS_TYPE.value()));
                params.setArchitecture(imageProps.get(ImagePropertyItem.ARCHITECTURE.value()));
            }
        }

        OsType osType = OsType.value(params.getOsType());
        if (osType == null) {
            throw new GCloudException("0090104::ä¸æ¯ææ­¤æä½ç³»ç»ç±»å");
        }
        ImageArchitecture architecture = ImageArchitecture.value(params.getArchitecture());
        if (architecture == null) {
            throw new GCloudException("0090105::ä¸æ¯ææ­¤æ¶æ");
        }
        return this.getProviderOrDefault().createImage(params, currentUser);
    }

    @Override
    public void updateImage(String imageId, String imageName) throws GCloudException {
        Image image = imageDao.getById(imageId);
        if (image == null) {
            throw new GCloudException("0090202::æ¾ä¸å°å¯¹åºçéå");
        }
        this.checkAndGetProvider(image.getProvider()).updateImage(imageId, image.getProviderRefId(), imageName);
        CacheContainer.getInstance().put(CacheType.IMAGE_NAME, imageId, imageName);
    }

    @Override
    public void deleteImage(String imageId) throws GCloudException {
        Image image = imageDao.getById(imageId);
        if (image == null) {
            throw new GCloudException("0090302::æ¾ä¸å°å¯¹åºçéå");
        }
        if(!image.getStatus().equals(ImageStatus.ACTIVE.value())) {
        	throw new GCloudException("::è¯¥ç¶æä¸çéåä¸è½å é?");
        }
        if (instanceDao.hasImageInstance(imageId)) {
            throw new GCloudException("0090303::æäºæå¡å¨æ­£å¨ä½¿ç¨æ­¤éåï¼ä¸è½å é?");
        }
        this.checkAndGetProvider(image.getProvider()).deleteImage(imageId, image.getProviderRefId());
    }

    @Override
    public Map<String, String> getImageProperties(String imageId) {
        Map<String, String> props = null;
        List<ImageProperty> imageProperties = imagePropertyDao.findByProperty(ImageProperty.IMAGE_ID, imageId);
        if (imageProperties != null && imageProperties.size() > 0) {
            props = new HashMap<>();
            for (ImageProperty imageProperty : imageProperties) {
                props.put(imageProperty.getName(), imageProperty.getValue());
            }
        }
        return props;
    }

    @Override
    public PageResult<ImageType> describeImage(DescribeImageParams params, CurrentUser currentUser) {
    	PageResult<ImageType> imageList = imageDao.describeDisks(params, ImageType.class, currentUser);
    	for (ImageType item : imageList.getList()) {
			item.setCnStatus(ImageStatus.getCnName(item.getStatus()));
		}
    	
        return imageList;
    }
    
    @Override
    public GenDownloadVo genDownload(String imageId) {
        Image image = imageDao.getById(imageId);
        if (image == null) {
            throw new GCloudException("0090302::æ¾ä¸å°å¯¹åºçéå");
        }
        return this.checkAndGetProvider(image.getProvider()).genDownload(imageId, image.getProviderRefId());
    }

    private IImageProvider getProviderOrDefault() {
        IImageProvider provider = ResourceProviders.getDefault(ResourceType.IMAGE);
        return provider;
    }

    private IImageProvider checkAndGetProvider(Integer providerType) {
        IImageProvider provider = ResourceProviders.checkAndGet(ResourceType.IMAGE, providerType);
        return provider;
    }

	@Override
	public void disableImage(String imageId, Boolean disable) {
		Image image = imageDao.getById(imageId);
        if (image == null) {
            throw new GCloudException("0090403::æ¾ä¸å°å¯¹åºçéå");
        }
        List<String> updateFields = new ArrayList<String>();
        updateFields.add(image.updateDisable(disable));
        
        imageDao.update(image, updateFields);
	}

	@Override
	public List<ImageStatisticsItem> imageStatistics() {
		return imageDao.imageStatistics(ImageStatisticsItem.class);
	}

	@Override
	public DetailImage detailImage(DetailImageParams params, CurrentUser currentUser) {
		DetailImage response = new DetailImage();
		Image image = imageDao.getById(params.getImageId());
		if(null == image) {
			return response;
		}
		
		response.setCnStatus(ImageStatus.getCnName(image.getStatus()));
		response.setCreateTime(image.getCreatedAt());
		response.setId(image.getId());
		//TODO æ°æ®æ¥æº?
//		response.setIsTask(false);
		response.setName(image.getName());
		
		//ä»gc_image_propertiesè¡¨ä¸­è·å
		Map<String, Object> propertyParams = new HashMap<>();
		propertyParams.put(ImageProperty.IMAGE_ID, params.getImageId());
		List<ImageProperty> properties = imagePropertyDao.findByProperties(propertyParams);
		for (ImageProperty item : properties) {
			switch (item.getName()) {
				case "os_type":
					response.setOsType(item.getValue());
					break;
				case "architecture":
					response.setArchiteture(item.getValue());
					break;
				case "description":
					response.setDescription(item.getValue());
					break;
				default:
					break;
			}
		}
		response.setSize(image.getMinDisk());
		response.setStatus(image.getStatus());
        response.setActualSize(image.getSize());
        response.setParentId(image.getParentId());
        response.setDisable(image.getDisable());
        response.setOwner(image.getOwner());
		response.setImageOwnerAlias(CacheContainer.getInstance().getString(CacheType.USER_NAME, image.getOwner()));
		return response;
	}

	@Override
	public Image getById(String imageId) {
		return imageDao.getById(imageId);
	}

    @Override
    public Resource download(String imageId) {

        Image image = imageDao.getById(imageId);
        if(image == null){
            throw new GCloudException("::éåä¸å­å?");
        }

        return this.checkAndGetProvider(image.getProvider()).download(imageId);
    }

    @Override
    public String upload(UploadImageParams params) {

        if(params.getParentId() != null){
            Image parentImage = imageDao.getById(params.getParentId());
            if(parentImage == null){
                throw new GCloudException("::æ¯éåä¸å­å¨");
            }
        }

        return this.getProviderOrDefault().uploadImage(params, params.getCurrentUser());
    }
}