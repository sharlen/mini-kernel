package com.gcloud.controller.image.entity.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum IsoTypeEnum {
	SYSTEM("系统光盘"),
	OTHER("其他");
	
	private String cnIsoTYpe;
	
	IsoTypeEnum(String cnIsoTYpe){
		this.cnIsoTYpe = cnIsoTYpe;
	}
	
	public String value() {
		return this.name().toLowerCase();
	}
	
	public static String getCnIsoType(String value) {
		IsoTypeEnum isoType = Arrays.stream(IsoTypeEnum.values()).filter(state -> state.value().equals(value)).findFirst().orElse(null);
		return isoType != null ? isoType.getCnIsoTYpe() : null;
	}

	public String getCnIsoTYpe() {
		return cnIsoTYpe;
	}

	public void setCnIsoTYpe(String cnIsoTYpe) {
		this.cnIsoTYpe = cnIsoTYpe;
	}
}