package com.gcloud.controller.image.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_vm_iso_attachments", jdbc = "controllerJdbcTemplate")
public class VmIsoAttachment {
    @ID
    private Long id;
    private String isoId;
    private String instanceId;
    private String mountpoint;
    private String status;
    private Date createdAt;
    private String isoPoolId;
    private String isoPool;
    private String isoStorageType;

    public static final String ID = "id";
    public static final String ISO_ID = "isoId";
    public static final String INSTANCE_ID = "instanceId";
    public static final String MOUNTPOINT = "mountpoint";
    public static final String STATUS = "status";
    public static final String CREATED_AT = "createdAt";
    public static final String ISO_POOL_ID = "isoPoolId";
    public static final String ISO_POOL = "isoPool";
    public static final String ISO_STORAGE_TYPE = "isoStorageType";

    public String updateId(Long id) {
        this.setId(id);
        return ID;
    }

    public String updateIsoId(String isoId) {
        this.setIsoId(isoId);
        return ISO_ID;
    }

    public String updateInstanceId(String instanceId) {
        this.setInstanceId(instanceId);
        return INSTANCE_ID;
    }

    public String updateMountpoint(String mountpoint) {
        this.setMountpoint(mountpoint);
        return MOUNTPOINT;
    }

    public String updateStatus(String status) {
        this.setStatus(status);
        return STATUS;
    }

    public String updateCreatedAt(Date createdAt) {
        this.setCreatedAt(createdAt);
        return CREATED_AT;
    }

    public String updateIsoPoolId(String isoPoolId) {
        this.setIsoPoolId(isoPoolId);
        return ISO_POOL_ID;
    }

    public String updateIsoPool(String isoPool) {
        this.setIsoPool(isoPool);
        return ISO_POOL;
    }

    public String updateIsoStorageType(String isoStorageType) {
        this.setIsoStorageType(isoStorageType);
        return ISO_STORAGE_TYPE;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsoId() {
        return isoId;
    }

    public void setIsoId(String isoId) {
        this.isoId = isoId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getMountpoint() {
        return mountpoint;
    }

    public void setMountpoint(String mountpoint) {
        this.mountpoint = mountpoint;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getIsoPool() {
        return isoPool;
    }

    public void setIsoPool(String isoPool) {
        this.isoPool = isoPool;
    }

    public String getIsoStorageType() {
        return isoStorageType;
    }

    public void setIsoStorageType(String isoStorageType) {
        this.isoStorageType = isoStorageType;
    }

    public String getIsoPoolId() {
        return isoPoolId;
    }

    public void setIsoPoolId(String isoPoolId) {
        this.isoPoolId = isoPoolId;
    }
}