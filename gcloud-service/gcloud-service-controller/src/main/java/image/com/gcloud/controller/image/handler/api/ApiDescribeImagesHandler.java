package com.gcloud.controller.image.handler.api;

import com.gcloud.controller.image.model.DescribeImageParams;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.model.ImageType;
import com.gcloud.header.image.msg.api.ApiDescribeImagesMsg;
import com.gcloud.header.image.msg.api.ApiDescribeImagesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DescribeImages", name = "镜像列表")
public class ApiDescribeImagesHandler extends MessageHandler<ApiDescribeImagesMsg, ApiDescribeImagesReplyMsg> {

    @Autowired
    private IImageService imageService;

    @Override
    public ApiDescribeImagesReplyMsg handle(ApiDescribeImagesMsg msg) throws GCloudException {

        DescribeImageParams params = BeanUtil.copyProperties(msg, DescribeImageParams.class);
        if(msg.getDisable() == null) {
        	params.setDisable(null);
        }
        PageResult<ImageType> response = imageService.describeImage(params, msg.getCurrentUser());
        ApiDescribeImagesReplyMsg reply = new ApiDescribeImagesReplyMsg();
        reply.init(response);

        return reply;
    }
}