package com.gcloud.controller.image.driver;

import com.gcloud.controller.image.dao.ImageStoreDao;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.service.common.enums.ImageStoreStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
public class RbdStoreDriver  implements IImageStoreDriver {
	@Autowired
	private ImageStoreDao storeDao;

	@Override
	public void copyImage(String sourceFilePath, String imageId, String resourceType) {
		//rbd copy 到images�?
		
		ImageStore store = new ImageStore();
		store.setImageId(imageId);
		store.setStoreTarget("images");//后续通过配置文件配置
		store.setStoreType(ImageDriverEnum.RBD.getStorageType());
		store.setStatus(ImageStoreStatus.ACTIVE.value());
		
		storeDao.save(store);
	}

	@Override
	public void deleteImage(String imageId, String resourceType) {
		//rbd delete image
	}

	@Override
	public void distributeImage(String imageId, String target, String targetType, String taskId, String resourceType,String distributeAction) {
		//file store in ceph
		//distribute to node or dd to vg
	}

	@Override
	public long getImageActualSize(String imageId) {
		return 0;
	}

	/*@Override
	public void deleteImageCache(String imageId) {
		
	}*/

	@Override
	public Resource download(String imageId) {
		return null;
	}


	@Override
	public void upload(String imageId, MultipartFile file) {

	}
}