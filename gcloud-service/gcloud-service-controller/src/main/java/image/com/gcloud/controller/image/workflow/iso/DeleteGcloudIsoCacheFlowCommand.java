package com.gcloud.controller.image.workflow.iso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.workflow.model.iso.DeleteGcloudIsoCacheFlowCommandReq;
import com.gcloud.controller.storage.dao.StorageLvNodeDao;
import com.gcloud.controller.storage.entity.StorageLvNode;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.msg.node.DeleteImageMsg;
import com.gcloud.service.common.lvm.uitls.LvmUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class DeleteGcloudIsoCacheFlowCommand extends BaseWorkFlowCommand{
	@Autowired
	MessageBus bus;
	
	@Autowired
    private StorageLvNodeDao storageLvNodeDao;
	
	@Override
	public boolean judgeExecute() {
		DeleteGcloudIsoCacheFlowCommandReq req = (DeleteGcloudIsoCacheFlowCommandReq)getReqParams();
		return req.getRepeatParams() != null;
	}

	@Override
	protected Object process() throws Exception {
		DeleteGcloudIsoCacheFlowCommandReq req = (DeleteGcloudIsoCacheFlowCommandReq)getReqParams();
		ImageStore store = req.getRepeatParams();
		DeleteImageMsg deleteMsg = new DeleteImageMsg();
		
		deleteMsg.setServiceId(store.getStoreType().equals("node")?MessageUtil.imageServiceId(store.getStoreTarget()):MessageUtil.imageServiceId(getImageNode(store.getStoreTarget(), store.getImageId())));
		deleteMsg.setImageId(store.getImageId());
		deleteMsg.setStoreTarget(store.getStoreTarget());
		deleteMsg.setStoreType(store.getStoreType());
		deleteMsg.setResourceType(ImageResourceType.ISO.name());
		deleteMsg.setTaskId(getTaskId());
        
        bus.send(deleteMsg);
		return null;
	}
	
	private String getImageNode(String poolName, String imageId) {
		List<StorageLvNode> storageLvNodes = storageLvNodeDao.getLvNodes(LvmUtil.getImagePath(poolName, imageId));
		if(storageLvNodes.size() > 0) {
			return storageLvNodes.get(0).getHostname();
		} else {
			String controllerService = MessageUtil.controllerServiceId();
			return controllerService.substring(controllerService.indexOf("-") + 1);
		}
	}

	@Override
	protected Object rollback() throws Exception {
		//è¿éé?è¦åæ»åï¼å¦æä¸åæ»ï¼åmapålnä¹ä¸é?è¦åæ»?
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return DeleteGcloudIsoCacheFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}