package com.gcloud.controller.image.handler.api.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.service.IIsoService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.iso.ApiModifyIsoAttributeMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect = "修改映像属�??")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="ModifyIsoAttribute",name="修改映像属�??")
public class ApiModifyIsoAttributeHandler extends MessageHandler<ApiModifyIsoAttributeMsg, ApiReplyMessage>{
	@Autowired
	private IIsoService isoService;
	
	@Override
	public ApiReplyMessage handle(ApiModifyIsoAttributeMsg msg) throws GCloudException {
		isoService.updateIso(msg.getIsoId(), msg.getIsoName());
        msg.setObjectId(msg.getIsoId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.ISO_NAME, msg.getIsoId()));
        return new ApiReplyMessage();
	}

}