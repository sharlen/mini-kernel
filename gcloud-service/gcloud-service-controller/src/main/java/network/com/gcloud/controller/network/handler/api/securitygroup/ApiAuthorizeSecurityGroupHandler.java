package com.gcloud.controller.network.handler.api.securitygroup;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.AuthorizeSecurityGroupMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建安全组规�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="AuthorizeSecurityGroup")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupId")
public class ApiAuthorizeSecurityGroupHandler extends MessageHandler<AuthorizeSecurityGroupMsg, ApiReplyMessage> {
	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public ApiReplyMessage handle(AuthorizeSecurityGroupMsg msg) throws GCloudException {
		AuthorizeSecurityGroupParams params = BeanUtil.copyProperties(msg, AuthorizeSecurityGroupParams.class);
		if(params != null && "all".equals(params.getIpProtocol())){
			params.setIpProtocol(null);
		}
		securityGroupService.authorizeSecurityGroup(params, msg.getCurrentUser());
		
		msg.setObjectId(msg.getSecurityGroupId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.SECURITYGROUP_NAME, msg.getSecurityGroupId()));
		return new ApiReplyMessage();
	}

}