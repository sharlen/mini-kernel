package com.gcloud.controller.network.handler.api.securitygroup;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.DescribeSecurityGroupAttributeResponse;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.DescribeSecurityGroupAttributeMsg;
import com.gcloud.header.network.msg.api.DescribeSecurityGroupAttributeReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="DescribeSecurityGroupAttribute", name = "安全组规则列�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupId")
public class ApiDescribeSecurityGroupAttributeHandler extends MessageHandler<DescribeSecurityGroupAttributeMsg, DescribeSecurityGroupAttributeReplyMsg> {
	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public DescribeSecurityGroupAttributeReplyMsg handle(DescribeSecurityGroupAttributeMsg msg) throws GCloudException {
		DescribeSecurityGroupAttributeResponse res = securityGroupService.describeSecurityGroupAttribute(msg.getSecurityGroupId(), msg.getDirection(), msg.getEtherType(), msg.getRegion());
		return BeanUtil.copyProperties(res, DescribeSecurityGroupAttributeReplyMsg.class);
	}

}