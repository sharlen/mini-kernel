package com.gcloud.controller.network.service;

import com.gcloud.controller.network.entity.QosPolicy;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IQosPolicyService {

    QosPolicy create(Integer providerType, String name, String description, Boolean isDefault, Boolean shared);

    void delete(String id);

    void updateQosLimit(String policyId, Integer egress, Integer ingress);

    QosPolicy createQosLimit(Integer provider, Integer egress, Integer ingress);
}