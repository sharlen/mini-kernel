package com.gcloud.controller.network.handler.sdk.securitygroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.CreateSecurityGroupParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.network.msg.sdk.SdkCreateTenantSecurityGroupMsg;
import com.gcloud.header.network.msg.sdk.SdkCreateTenantSecurityGroupReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class SdkCreateTenantSecurityGroupHandler extends MessageHandler<SdkCreateTenantSecurityGroupMsg, SdkCreateTenantSecurityGroupReplyMsg> {
	@Autowired
	ISecurityGroupService securityGroupService;

	@Override
	public SdkCreateTenantSecurityGroupReplyMsg handle(SdkCreateTenantSecurityGroupMsg msg) throws GCloudException {
		CreateSecurityGroupParams params = BeanUtil.copyProperties(msg, CreateSecurityGroupParams.class);
		params.setDefaultSg(true);
		SdkCreateTenantSecurityGroupReplyMsg reply = new SdkCreateTenantSecurityGroupReplyMsg();
		reply.setSecurityGroupId(securityGroupService.createSecurityGroup(params, msg.getCurrentUser()));
		
		return reply;
	}

}