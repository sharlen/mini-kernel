package com.gcloud.controller.network.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.common.model.PortRangeInfo;
import com.gcloud.common.util.NetworkUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.SecurityGroupDao;
import com.gcloud.controller.network.dao.SecurityGroupPortBindingDao;
import com.gcloud.controller.network.dao.SecurityGroupRuleDao;
import com.gcloud.controller.network.entity.SecurityGroup;
import com.gcloud.controller.network.entity.SecurityGroupPortBinding;
import com.gcloud.controller.network.entity.SecurityGroupRule;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.model.CreateSecurityGroupParams;
import com.gcloud.controller.network.model.DescribeSecurityGroupAttributeResponse;
import com.gcloud.controller.network.model.DescribeSecurityGroupsParams;
import com.gcloud.controller.network.model.ModifySecurityGroupAttributeParams;
import com.gcloud.controller.network.provider.ISecurityGroupProvider;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.controller.utils.OrderBy;
import com.gcloud.controller.utils.UserUtil;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.EtherType;
import com.gcloud.header.compute.enums.NetProtocol;
import com.gcloud.header.compute.enums.SecurityDirection;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.model.DetailSecurityGroupResponse;
import com.gcloud.header.network.model.PermissionType;
import com.gcloud.header.network.model.PermissionTypes;
import com.gcloud.header.network.model.SecurityGroupItemType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SecurityGroupServiceImpl implements ISecurityGroupService {
    @Autowired
    private SecurityGroupDao securityGroupDao;

    @Autowired
    private SecurityGroupPortBindingDao securityGroupPortBindingDao;
    
    @Autowired
    private SecurityGroupRuleDao securityGroupRuleDao;

    @Override
    public PageResult<SecurityGroupItemType> describeSecurityGroups(DescribeSecurityGroupsParams params, CurrentUser currentUser) {
    	PageResult<SecurityGroupItemType> page = securityGroupDao.describeSecurityGroups(params, SecurityGroupItemType.class, currentUser);
    	
    	//初始化创建�?�名�?
    	List<SecurityGroupItemType> list = page.getList();
    	for (SecurityGroupItemType item : list) {
			if(StringUtils.isNotBlank(item.getUserId())) {
				item.setCreator(UserUtil.userName(item.getUserId()));
			}
		}
        return page;
    }

    @Override
    public String createSecurityGroup(CreateSecurityGroupParams params, CurrentUser currentUser) throws GCloudException {
        return this.getProviderOrDefault(params.getProvider()).createSecurityGroup(params, currentUser);
    }

    @Override
    public void deleteSecurityGroup(String id, Boolean force) throws GCloudException {
    	log.info("deleteSecurityGroup_isForce_" + force);
        SecurityGroup sg = securityGroupDao.getById(id);
        if (null == sg) {
            throw new GCloudException("0040402::找不到安全组");
        }

        SecurityGroupPortBinding binding = securityGroupPortBindingDao.findOneByProperty(SecurityGroupPortBinding.SECURITY_GROUP_ID, sg.getId());
        if(binding != null){
            throw new GCloudException("0040404::安全组正在使用，不能删除");
        }

        if(sg.getIsDefault() != null && sg.getIsDefault() && !force){
            throw new GCloudException("0040403::默认安全组不能删�?");
        }

        this.checkAndGetProvider(sg.getProvider()).deleteSecurityGroup(sg);
    }

    @Override
    public void modifySecurityGroupAttribute(ModifySecurityGroupAttributeParams params) {
        SecurityGroup sg = securityGroupDao.getById(params.getSecurityGroupId());
        if (null == sg) {
            throw new GCloudException("0040202::找不到安全组");
        }

        if(sg.getIsDefault() != null && sg.getIsDefault()){
            throw new GCloudException("0040205::默认安全组不能修�?");
        }

        this.checkAndGetProvider(sg.getProvider()).modifySecurityGroup(params, sg);
        
        CacheContainer.getInstance().put(CacheType.SECURITYGROUP_NAME, params.getSecurityGroupId(), params.getSecurityGroupName());
    }

    @Override
    public void authorizeSecurityGroup(AuthorizeSecurityGroupParams params, CurrentUser currentUser) {
        SecurityGroup sg = securityGroupDao.getById(params.getSecurityGroupId());
        if (null == sg) {
            throw new GCloudException("0040504::找不到安全组");
        }
        params.setSecurityGroupRefId(sg.getProviderRefId());
        if (StringUtils.isNotBlank(params.getSourceGroupId())) {
            SecurityGroup source = securityGroupDao.getById(params.getSourceGroupId());
            if (null == source) {
                throw new GCloudException("0040505::找不到源安全�?");
            }
            params.setSourceGroupRefId(source.getProviderRefId());
        }
        if (StringUtils.isNotBlank(params.getDestGroupId())) {
            SecurityGroup dest = securityGroupDao.getById(params.getDestGroupId());
            if (null == dest) {
                throw new GCloudException("0040506::找不到目标安全组");
            }
            params.setDestGroupRefId(dest.getProviderRefId());
        }

        SimpleFlowChain<String, String> chain = new SimpleFlowChain<>("create sg rule");
        chain.then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                String ruleProviderRefId = checkAndGetProvider(sg.getProvider()).authorizeSecurityGroup(params);
                chain.data(ruleProviderRefId);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                checkAndGetProvider(sg.getProvider()).revokeSecurityGroup(sg.getProviderRefId(), data);
                chain.rollback();
            }
        }).then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                createSecurityGroupRule(params, currentUser, sg, data);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                chain.rollback();
            }
        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }

    }
    
    private String createSecurityGroupRule(AuthorizeSecurityGroupParams params, CurrentUser currentUser, SecurityGroup sg, String ruleRefId) {
    	SecurityGroupRule rule = new SecurityGroupRule();
    	rule.setCreateTime(new Date());
    	rule.setDirection(params.getDirection());
    	rule.setEthertype(EtherType.IPv4.getValue());
    	rule.setId(UUID.randomUUID().toString());
    	PortRangeInfo portRangeInfo = NetworkUtil.portRangeInfo(params.getPortRange());
    	if(portRangeInfo == null){
    	    throw new GCloudException("::端口范围格式不正�?");
        }

    	if(params.getIpProtocol() != null){
            NetProtocol netProtocol = NetProtocol.value(params.getIpProtocol());
            if(netProtocol == null){
            	//协议类型为all的情况，没有枚举
            	if("all".equalsIgnoreCase(params.getIpProtocol())) {
            		rule.setPortRangeMin(-1);
            		rule.setPortRangeMax(-1);
            		rule.setProtocol(null);
            	} else {
            		throw new GCloudException("::不支持此协议");
            	}
            } else {
            	rule.setProtocol(params.getIpProtocol());
            }
        }

        //判断在此之前是否赋�?�了
        if(rule.getPortRangeMin() == null) {
        	rule.setPortRangeMin(portRangeInfo.getMin());
        }
        if(rule.getPortRangeMax() == null) {
        	rule.setPortRangeMax(portRangeInfo.getMax());
        }
        
    	rule.setProvider(sg.getProvider());
    	rule.setProviderRefId(ruleRefId);
    	if(params.getDirection().equals(SecurityDirection.EGRESS.value())) {
    		if(com.gcloud.common.util.StringUtils.isNotBlank(params.getDestGroupId())) {
    			rule.setRemoteGroupId(params.getDestGroupId());
			}
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getDestCidrIp())) {
				rule.setRemoteIpPrefix(params.getDestCidrIp());
			}
    	} else {
    		if(com.gcloud.common.util.StringUtils.isNotBlank(params.getSourceGroupId())) {
    			rule.setRemoteGroupId(params.getSourceGroupId());
			}
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getSourceCidrIp())) {
				rule.setRemoteIpPrefix(params.getSourceCidrIp());
			}
    	}
    	rule.setSecurityGroupId(sg.getId());
    	rule.setTenantId(currentUser.getDefaultTenant());
    	rule.setUserId(currentUser.getId());
    	
    	securityGroupRuleDao.save(rule);
    	
    	return rule.getId();
    }

    @Override
    public void revokeSecurityGroup(String securityGroupRuleId) {
        SecurityGroupRule sgRule = securityGroupRuleDao.getById(securityGroupRuleId);
        if (null == sgRule) {
            throw new GCloudException("0040602::找不到安全组规则");
        }
        
        SecurityGroup sg = securityGroupDao.getById(sgRule.getSecurityGroupId());
        if (null == sg) {
            throw new GCloudException("0040603::找不到安全组");
        }
        securityGroupRuleDao.deleteById(securityGroupRuleId);
        
        this.checkAndGetProvider(sg.getProvider()).revokeSecurityGroup(sg.getProviderRefId(), sgRule.getProviderRefId());
    }

    @Override
    public DescribeSecurityGroupAttributeResponse describeSecurityGroupAttribute(String securityGroupId, String direction, String etherType, String regionId) {
        SecurityGroup sGroup = securityGroupDao.getById(securityGroupId);
        if (null == sGroup) {
            throw new GCloudException("0040702::找不到安全组");
        }
        
		DescribeSecurityGroupAttributeResponse response = new DescribeSecurityGroupAttributeResponse();
		PermissionTypes permissionTypes = new PermissionTypes();

		response.setDescription(sGroup.getDescription());
		response.setRegionId(regionId);
		response.setSecurityGroupId(sGroup.getId());
		response.setSecurityGroupName(sGroup.getName());
		//添加创建时间和创建�??
		response.setCreateTime(sGroup.getCreateTime());
		String userName = UserUtil.userName(sGroup.getUserId());
		response.setCreator(userName);
		
		Map<String, Object> pars = new HashMap<String, Object>();
		pars.put(SecurityGroupRule.SECURITY_GROUP_ID, securityGroupId);

		if(StringUtils.isNotBlank(direction)){
            SecurityDirection securityDirection = SecurityDirection.value(direction);
            if(securityDirection == null) {
                throw new GCloudException("::不支持此direction");

            }
            pars.put(SecurityGroupRule.DIRECTION, securityDirection.value());
        }

		if(StringUtils.isNotBlank(etherType)){
            EtherType ethType = EtherType.getByValue(etherType);
            if(ethType == null){
                throw new GCloudException("::不支持此etherType");
            }
            pars.put(SecurityGroupRule.ETHERTYPE, ethType.getValue());
        }


		
		List<SecurityGroupRule> rules = securityGroupRuleDao.findByProperties(pars, SecurityGroupRule.CREATE_TIME, OrderBy.OrderType.DESC.value());

		for (SecurityGroupRule rule : rules) {
			PermissionType type = new PermissionType();
			type.setSecurityGroupRuleId(rule.getId());
			type.setIpProtocol(rule.getProtocol());
			type.setDirection(rule.getDirection());

			type.setPortRange(NetworkUtil.porRange(rule.getPortRangeMin(), rule.getPortRangeMax()));

			if (rule.getDirection() != null) {
				//将remoteGroupId转换成groupName;
				String remoteGroupId = rule.getRemoteGroupId();
				String remoteGroupName = null;
				if(StringUtils.isNotBlank(remoteGroupId)) {
					SecurityGroup sg = securityGroupDao.getById(remoteGroupId);
					if(null != sg) {
						remoteGroupName = sg.getName();
					}
				}
				
				if(rule.getDirection().equals(SecurityDirection.EGRESS.value())) {
					type.setDestGroupId(rule.getRemoteGroupId());
					//填充数据
					type.setDestGroupName(remoteGroupName);
					type.setDestCidrIp(rule.getRemoteIpPrefix());
				} else if(rule.getDirection().equals(SecurityDirection.INGRESS.value())) {
					type.setSourceGroupId(rule.getRemoteGroupId());
					//填充数据
					type.setSourceGroupName(remoteGroupName);
					type.setSourceCidrIp(rule.getRemoteIpPrefix());
				}
			}

			permissionTypes.getPermission().add(type);
		}

		response.setPermissions(permissionTypes);
		
		return response;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void cleanSecurityGroupData(String id) {
        securityGroupDao.deleteById(id);
        securityGroupPortBindingDao.deleteBySecurityGroup(id);
        securityGroupRuleDao.deleteBySecurityGroup(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public SecurityGroup createSecurityGroupData(CreateSecurityGroupParams params, ProviderType provider, String refSecurityGroupId, CurrentUser currentUser) {
        SecurityGroup sg = new SecurityGroup();
        sg.setId(UUID.randomUUID().toString());
        sg.setCreateTime(new Date());
        sg.setDescription(params.getDescription());
        sg.setName(params.getSecurityGroupName());
        sg.setUserId(currentUser.getId());
        sg.setTenantId(currentUser.getDefaultTenant());
        sg.setProvider(provider.getValue());
        sg.setProviderRefId(refSecurityGroupId);
        if(params.isDefaultSg()) {
        	sg.setIsDefault(true);//租户默认安全�?
        } else {
        	sg.setIsDefault(false);
        }
        securityGroupDao.save(sg);

        return sg;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void modifySecurityGroupData(ModifySecurityGroupAttributeParams params) {
        SecurityGroup sg = new SecurityGroup();
        sg.setId(params.getSecurityGroupId());

        List<String> updateField = new ArrayList<>();
        if (params.getSecurityGroupName() != null) {
            updateField.add(sg.updateName(params.getSecurityGroupName()));
        }

        if (params.getDescription() != null) {
            updateField.add(sg.updateDescription(params.getDescription()));
        }

        if (updateField.size() > 0) {
            securityGroupDao.update(sg, updateField);
        }
    }

    private ISecurityGroupProvider getProviderOrDefault(Integer providerType) {
        ISecurityGroupProvider provider = ResourceProviders.getOrDefault(ResourceType.SECURITY_GROUP, providerType);
        return provider;
    }

    private ISecurityGroupProvider checkAndGetProvider(Integer providerType) {
        ISecurityGroupProvider provider = ResourceProviders.checkAndGet(ResourceType.SECURITY_GROUP, providerType);
        return provider;
    }

	@Override
	public DetailSecurityGroupResponse detail(String securityGroupId) {
		DetailSecurityGroupResponse res = new DetailSecurityGroupResponse();
		SecurityGroup sg = securityGroupDao.getById(securityGroupId);
        if (null == sg) {
            throw new GCloudException("0040802::找不到安全组");
        }
        res.setCreateTime(sg.getCreateTime());
        res.setDescription(sg.getDescription());
        res.setSecurityGroupId(securityGroupId);
        res.setSecurityGroupName(sg.getName());
        
        //添加创建者返�?
        String userId = sg.getUserId();
        String userName = (StringUtils.isNotBlank(userId)) ? UserUtil.userName(userId) : null;
        res.setCreator(userName);
		return res;
	}

}