package com.gcloud.controller.network.handler.api.externalnetwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ModifyExternalNetworkAttributeMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="修改外网属�??")
@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="ModifyExternalNetworkAttribute",name="修改外网属�??")

public class ApiModifyExternalNetworkAttributeHandler extends MessageHandler<ModifyExternalNetworkAttributeMsg, ApiReplyMessage>{

	@Autowired
	private INetworkService service;
	
	@Override
	public ApiReplyMessage handle(ModifyExternalNetworkAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		ModifyVpcAttributeMsg vpcMsg = BeanUtil.copyProperties(msg, ModifyVpcAttributeMsg.class);
		vpcMsg.setVpcId(msg.getNetworkId());
		vpcMsg.setVpcName(msg.getNetworkName());
		service.updateNetwork(vpcMsg);
		
		msg.setObjectId(msg.getNetworkId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.NETWORK_NAME, msg.getNetworkId()));
		return new ApiReplyMessage();
	}

}