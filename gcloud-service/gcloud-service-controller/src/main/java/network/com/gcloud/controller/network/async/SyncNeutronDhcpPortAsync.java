package com.gcloud.controller.network.async;

import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.controller.network.provider.enums.neutron.NeutronDeviceOwner;
import com.gcloud.controller.network.util.NeutronSubnetDhcpUtil;
import com.gcloud.controller.network.util.SubnetDhcpHandleInfo;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.core.async.AsyncBase;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.async.NetworkThreadPool;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.openstack4j.model.network.IP;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.model.network.options.PortListOptions;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class SyncNeutronDhcpPortAsync extends AsyncBase {

    private String neutronSubnetId;
    private String gcloudSubnetId;
    private String handleId;

    private SubnetDhcpHandle subnetDhcpHandle;
    private String neutronNetworkId;
    private Boolean canRun;

    private long timeout = 120000L;
    private int dhcpAgentNum = 1;

    private Boolean dhcp;

    public SyncNeutronDhcpPortAsync() {
        init();
    }

    public SyncNeutronDhcpPortAsync(String gcloudSubnetId, String neutronSubnetId, String handleId) {
        this.neutronSubnetId = neutronSubnetId;
        this.gcloudSubnetId = gcloudSubnetId;
        this.handleId = handleId;

        init();
    }

    public void init(){

        //用网络的pool，和async base的pool分开
        NetworkThreadPool pool = SpringUtil.getBean(NetworkThreadPool.class);
        super.setThreadPool(pool);

        Environment env = SpringUtil.getBean(Environment.class);
        String dhcpAgentNumStr = env.getProperty("gcloud.controller.network.dhcpAgentPerNetwork");
        try{
            dhcpAgentNum = Integer.valueOf(dhcpAgentNumStr);
        }catch (Exception ex){
        }
    }

    @Override
    public long timeout() {
        //根据dhcp数量设置 port �?个两分钟
        return dhcpAgentNum * timeout;
    }

    @Override
    public long sleepTime() {
        return 1000L;
    }

    @Override
    public AsyncResult execute() {

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] begin", gcloudSubnetId, handleId));

        NeutronSubnetDhcpUtil.running(gcloudSubnetId);

        SubnetDhcpHandleInfo info = NeutronSubnetDhcpUtil.canRun(gcloudSubnetId, handleId);
        subnetDhcpHandle = info.getSubnetDhcpHandle();

        canRun = info.isCanRun();
        if(!info.isCanRun()){
            throw new GCloudException("::已经被其他处�?");
        }

        if(subnetDhcpHandle == null){
            return new AsyncResult(AsyncStatus.FAILED);
        }

        NeutronProviderProxy proxy = SpringUtil.getBean(NeutronProviderProxy.class);

        //不在初始化时处理，因为并发问题，可能导致修改�?
        Subnet subnet = proxy.getSubnet(neutronSubnetId);

        if(dhcp == null){
            dhcp = subnet.isDHCPEnabled();
            //取消dhcp不超�?
            if(!subnet.isDHCPEnabled()){
                setNewTimeout(0L);
            }
        //发生了改变，如果handle变化了，后面插入数据的时候会停止。否则则修改超时时间
        }else if(dhcp != subnet.isDHCPEnabled()){

            if(!subnet.isDHCPEnabled()){
                setNewTimeout(0L);
            }else{
                setNewTimeout(dhcpAgentNum * timeout);
            }

        }
        neutronNetworkId = subnet.getNetworkId();

        PortListOptions options = PortListOptions.create().deviceOwner(NeutronDeviceOwner.DHCP.getNeutronDeviceOwner()).networkId(neutronNetworkId);

        List<? extends Port> ports = proxy.listPort(options);

        List<Port> foundPorts = new ArrayList<>();

        if(ports != null && !ports.isEmpty()){
            for(Port port : ports){
                for(IP fixIp : port.getFixedIps()){

                    if(!neutronSubnetId.equals(fixIp.getSubnetId())){
                        continue;
                    }
                    foundPorts.add(port);
                    break;
                }
            }
        }


        SubnetDhcpHandleInfo createInfo = NeutronSubnetDhcpUtil.syncDhcpPortData(gcloudSubnetId, handleId, foundPorts);
        subnetDhcpHandle = createInfo.getSubnetDhcpHandle();
        canRun = createInfo.isCanRun();

        if(!canRun){
            throw new GCloudException("::已经被其他处�?");
        }

        if(subnetDhcpHandle == null){
            return new AsyncResult(AsyncStatus.FAILED);
        }

        boolean finish = false;

        if(subnet.isDHCPEnabled()){
            if(foundPorts.size() >= dhcpAgentNum){
                finish = true;
            }
        }else{
            if(foundPorts.size() == 0){
                finish = true;
            }
        }

        if(finish){
            if(createInfo.isOperationSucc()){
                return new AsyncResult(AsyncStatus.SUCCEED);
                //都找到，但是同步失败
            }else{
                return new AsyncResult(AsyncStatus.FAILED);
            }
        }

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] end", gcloudSubnetId, handleId));

        return new AsyncResult(AsyncStatus.RUNNING);
    }


    @Override
    public void defaultHandler() {
        NeutronSubnetDhcpUtil.finishHandle(gcloudSubnetId, handleId);
    }

    @Override
    public void exceptionHandler() {

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] exception", gcloudSubnetId, handleId));
        //跑出异常，如果是不可运行，则不管
        if(canRun != null && !canRun){
            log.debug(String.format("dhcp sync subnet [%s], handle [%s] can not run", gcloudSubnetId, handleId));
            return;
        }

        NeutronSubnetDhcpUtil.finishHandle(gcloudSubnetId, handleId);

    }

    @Override
    public String outputString() {
        return String.format("CreateNeutronDhcpPortAsync dhcp sync subnet [%s], handle [%s]", gcloudSubnetId, handleId);
    }

    public String getNeutronNetworkId() {
        return neutronNetworkId;
    }

    public void setNeutronNetworkId(String neutronNetworkId) {
        this.neutronNetworkId = neutronNetworkId;
    }

    public String getNeutronSubnetId() {
        return neutronSubnetId;
    }

    public void setNeutronSubnetId(String neutronSubnetId) {
        this.neutronSubnetId = neutronSubnetId;
    }

}