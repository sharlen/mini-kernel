package com.gcloud.controller.network.model;

import com.gcloud.common.model.PageParams;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeNetworkInterfacesParams extends PageParams {
	private String vSwitchId;
	private String primaryIpAddress;
	private String securityGroupId;
	private String networkInterfaceName;
	private String instanceId;
	private List<String> networkInterfaceIds;
	private List<String> deviceOwners;
	private Boolean includeOwnerless;
	private Boolean avail;
	private Boolean eni;
	private Boolean isFixed;

	public Boolean getAvail() {
		return avail;
	}

	public void setAvail(Boolean avail) {
		this.avail = avail;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}

	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public List<String> getNetworkInterfaceIds() {
		return networkInterfaceIds;
	}

	public void setNetworkInterfaceIds(List<String> networkInterfaceIds) {
		this.networkInterfaceIds = networkInterfaceIds;
	}

	public List<String> getDeviceOwners() {
		return deviceOwners;
	}

	public void setDeviceOwners(List<String> deviceOwners) {
		this.deviceOwners = deviceOwners;
	}

	public Boolean getIncludeOwnerless() {
		return includeOwnerless;
	}

	public void setIncludeOwnerless(Boolean includeOwnerless) {
		this.includeOwnerless = includeOwnerless;
	}

	public Boolean getEni() {
		return eni;
	}

	public void setEni(Boolean eni) {
		this.eni = eni;
	}

	public Boolean getIsFixed() {
		return isFixed;
	}

	public void setIsFixed(Boolean isFixed) {
		this.isFixed = isFixed;
	}
}