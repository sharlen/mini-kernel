package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_qos_port_policy_bindings", jdbc = "controllerJdbcTemplate")
public class QosPortPolicyBinding {

    @ID
    private Long id;
    private String policyId;
    private String portId;

    public static final String ID = "id";
    public static final String POLICY_ID = "policyId";
    public static final String PORT_ID = "portId";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

	public String updateId(Long id) {
		this.setId(id);
		return ID;
	}

	public String updatePolicyId(String policyId) {
		this.setPolicyId(policyId);
		return POLICY_ID;
	}

	public String updatePortId(String portId) {
		this.setPortId(portId);
		return PORT_ID;
	}
}