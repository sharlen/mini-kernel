package com.gcloud.controller.network.async;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.SubnetDhcpHandleDao;
import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.controller.network.provider.enums.neutron.NeutronDeviceOwner;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.util.NeutronSubnetDhcpUtil;
import com.gcloud.controller.network.util.SubnetDhcpHandleInfo;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.core.async.AsyncBase;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.async.NetworkThreadPool;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.enums.ProviderType;
import lombok.extern.slf4j.Slf4j;
import org.openstack4j.model.network.IP;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.model.network.options.PortListOptions;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class CreateNeutronDhcpPortAsync extends AsyncBase {

    private String neutronSubnetId;
    private String gcloudSubnetId;
    private String handleId;
    private CurrentUser currentUser;

    private SubnetDhcpHandle subnetDhcpHandle;
    private String neutronNetworkId;
    private Boolean canRun;

    private long timeout = 120000L;
    private int dhcpAgentNum = 1;

    public CreateNeutronDhcpPortAsync() {
        init();
    }

    public CreateNeutronDhcpPortAsync(String gcloudSubnetId, String neutronSubnetId, String handleId, CurrentUser currentUser) {
        this.neutronSubnetId = neutronSubnetId;
        this.gcloudSubnetId = gcloudSubnetId;
        this.handleId = handleId;
        this.currentUser = currentUser;

        init();
    }

    public void init(){

        //用网络的pool，和async base的pool分开
        NetworkThreadPool pool = SpringUtil.getBean(NetworkThreadPool.class);
        super.setThreadPool(pool);

        Environment env = SpringUtil.getBean(Environment.class);
        String dhcpAgentNumStr = env.getProperty("gcloud.controller.network.dhcpAgentPerNetwork");
        try{
            dhcpAgentNum = Integer.valueOf(dhcpAgentNumStr);
        }catch (Exception ex){
        }
    }

    @Override
    public long timeout() {
        //根据dhcp数量设置 port �?个两分钟
        return dhcpAgentNum * timeout;
    }

    @Override
    public long sleepTime() {
        return 1000L;
    }

    @Override
    public AsyncResult execute() {

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] begin", gcloudSubnetId, handleId));

        NeutronSubnetDhcpUtil.running(gcloudSubnetId);

        SubnetDhcpHandleInfo info = NeutronSubnetDhcpUtil.canRun(gcloudSubnetId, handleId);
        subnetDhcpHandle = info.getSubnetDhcpHandle();

        canRun = info.isCanRun();
        if(!info.isCanRun()){
            throw new GCloudException("::已经被其他处�?");
        }

        if(subnetDhcpHandle == null){
            return new AsyncResult(AsyncStatus.FAILED);
        }

        NeutronProviderProxy proxy = SpringUtil.getBean(NeutronProviderProxy.class);

        if(StringUtils.isBlank(neutronNetworkId)){
            try{
                Subnet subnet = proxy.getSubnet(neutronSubnetId);
                neutronNetworkId = subnet.getNetworkId();
            }catch (Exception ex){
                log.error("::获取子网失败", ex);
            }

        }

        if(StringUtils.isBlank(neutronNetworkId)){
            return new AsyncResult(AsyncStatus.FAILED);
        }

        PortListOptions options = PortListOptions.create().deviceOwner(NeutronDeviceOwner.DHCP.getNeutronDeviceOwner()).networkId(neutronNetworkId);

        List<? extends Port> ports = proxy.listPort(options);

        if(ports == null || ports.isEmpty()){
            return new AsyncResult(AsyncStatus.RUNNING);
        }

        List<Port> foundPorts = new ArrayList<>();
        for(Port port : ports){


            for(IP fixIp : port.getFixedIps()){

                if(!neutronSubnetId.equals(fixIp.getSubnetId())){
                    continue;
                }

                foundPorts.add(port);
                break;
            }


        }

        SubnetDhcpHandleInfo createInfo = NeutronSubnetDhcpUtil.syncDhcpPortData(gcloudSubnetId, handleId, foundPorts);
        subnetDhcpHandle = createInfo.getSubnetDhcpHandle();
        canRun = createInfo.isCanRun();

        if(!canRun){
            throw new GCloudException("::已经被其他处�?");
        }

        if(subnetDhcpHandle == null){
            return new AsyncResult(AsyncStatus.FAILED);
        }


        if(foundPorts.size() >= dhcpAgentNum){
            if(createInfo.isOperationSucc()){
                return new AsyncResult(AsyncStatus.SUCCEED);
            //都找到，但是同步失败
            }else{
                return new AsyncResult(AsyncStatus.FAILED);
            }
        }

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] end", gcloudSubnetId, handleId));

        return new AsyncResult(AsyncStatus.RUNNING);
    }

    public void checkSubnet(){

        if(subnetDhcpHandle != null){
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);

            String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(gcloudSubnetId);
            String lockId = LockUtil.spinLock(lockKey, 5000L, 6000L);
            try{
                subnetDhcpHandle = dhcpHandlerDao.getById(gcloudSubnetId);
            }finally {
                LockUtil.releaseSpinLock(lockKey, lockId);
            }
        }

        //成功了，看看子网是否还存在，如果不存在，则被删了，可能是回滚删了
        //失败了也是判断子网是否存�?

        //把数据库删了,可能会和删除的并发删�?,没有关系
        if(subnetDhcpHandle == null){

            PortDao portDao = SpringUtil.getBean(PortDao.class);

            Map<String, Object> subnetFilter = new HashMap<>();
            subnetFilter.put(com.gcloud.controller.network.entity.Subnet.PROVIDER, ProviderType.NEUTRON.getValue());
            subnetFilter.put(com.gcloud.controller.network.entity.Subnet.PROVIDER_REF_ID, neutronNetworkId);

            List<com.gcloud.controller.network.entity.Port> ports = portDao.subnetPorts(gcloudSubnetId, DeviceOwner.DHCP.getValue());
            if(ports != null && !ports.isEmpty()){
                IPortService portService = SpringUtil.getBean(IPortService.class);
                for(com.gcloud.controller.network.entity.Port port : ports){
                    portService.cleanPortData(port.getId());
                }
            }
        }

    }

    @Override
    public void defaultHandler() {
        NeutronSubnetDhcpUtil.finishHandle(gcloudSubnetId, handleId);
    }

    @Override
    public void exceptionHandler() {

        log.debug(String.format("dhcp sync subnet [%s], handle [%s] exception", gcloudSubnetId, handleId));
        //跑出异常，如果是不可运行，则不管
        if(canRun != null && !canRun){
            log.debug(String.format("dhcp sync subnet [%s], handle [%s] can not run", gcloudSubnetId, handleId));
            return;
        }

        NeutronSubnetDhcpUtil.finishHandle(gcloudSubnetId, handleId);

    }

    @Override
    public String outputString() {
        return String.format("CreateNeutronDhcpPortAsync dhcp sync subnet [%s], handle [%s]", gcloudSubnetId, handleId);
    }

    public String getNeutronNetworkId() {
        return neutronNetworkId;
    }

    public void setNeutronNetworkId(String neutronNetworkId) {
        this.neutronNetworkId = neutronNetworkId;
    }

    public String getNeutronSubnetId() {
        return neutronSubnetId;
    }

    public void setNeutronSubnetId(String neutronSubnetId) {
        this.neutronSubnetId = neutronSubnetId;
    }

    public CurrentUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}