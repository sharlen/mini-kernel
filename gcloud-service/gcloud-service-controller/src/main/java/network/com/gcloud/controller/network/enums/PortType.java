package com.gcloud.controller.network.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum PortType {

	COMPUTE("compute:node"),FOREIGN("network:foreign");

	private String value;

	PortType(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static PortType getByValue(String value){
		return Arrays.stream(PortType.values()).filter(t -> t.getValue().equals(value)).findFirst().orElse(null);
	}
}