package com.gcloud.controller.network.handler.api.network;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.ApiDetailVpcMsg;
import com.gcloud.header.network.msg.api.ApiDetailVpcReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="DetailVpc",name="虚拟私有云详�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcId")
public class ApiDetailVpcHandler extends MessageHandler<ApiDetailVpcMsg, ApiDetailVpcReplyMsg>{
	@Autowired
	IVpcService service;
	
	@Override
	public ApiDetailVpcReplyMsg handle(ApiDetailVpcMsg msg) throws GCloudException {
		ApiDetailVpcReplyMsg reply  = new ApiDetailVpcReplyMsg();
		reply.setDetailVpc(service.detail(msg.getVpcId()));
		return reply;
	}

}