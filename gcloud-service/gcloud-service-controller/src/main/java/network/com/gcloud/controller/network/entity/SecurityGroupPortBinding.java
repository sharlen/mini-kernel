package com.gcloud.controller.network.entity;

/**
 * Created by yaowj on 2018/10/25.
 */

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_security_group_port_bindings", jdbc = "controllerJdbcTemplate")
public class SecurityGroupPortBinding {

    @ID
    private Long id;
    private String portId;
    private String securityGroupId;

    public static final String ID = "id";
    public static final String PORT_ID = "portId";
    public static final String SECURITY_GROUP_ID = "securityGroupId";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public String updateId(Long id) {
        this.setId(id);
        return ID;
    }

    public String updatePortId(String portId) {
        this.setPortId(portId);
        return PORT_ID;
    }

    public String updateSecurityGroupId(String securityGroupId) {
        this.setSecurityGroupId(securityGroupId);
        return SECURITY_GROUP_ID;
    }
}