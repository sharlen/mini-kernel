package com.gcloud.controller.network.service;

import com.gcloud.controller.network.entity.Router;
import com.gcloud.controller.network.model.DescribeVRoutersParams;
import com.gcloud.controller.network.model.DetailVRouterParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.DetailVRouter;
import com.gcloud.header.network.model.VRouterSetType;
import com.gcloud.header.network.msg.api.CreateVRouterMsg;
import com.gcloud.header.network.msg.api.ModifyVRouterAttributeMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IRouterService {
	String createVRouter(CreateVRouterMsg msg);
	Router getVRouterById(String vRouterId);
	void deleteVRouter(String routerId);
	PageResult<VRouterSetType> describeVRouters(DescribeVRoutersParams params, CurrentUser currentUser);
	void modifyVRouterAttribute(ModifyVRouterAttributeMsg msg);
	void setVRouterGateway(String vRouterId, String networkId, CurrentUser currentUser);
	void cleanVRouterGateway(String vRouterId);
	void attachVSwitchVRouter(String routerId, String subnetId, CurrentUser currentUser);
	void detachVSwitchVRouter(String routerId, String subnetId, String portId);
	void detachVSwitchVRouter(String routerId, String subnetId);
	int routerStatistics(CurrentUser currentUser);
	DetailVRouter detailVRouter(DetailVRouterParams params, CurrentUser currentUser);
}