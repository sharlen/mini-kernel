package com.gcloud.controller.network.handler.api.floatingip;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.EipAddressSetType;
import com.gcloud.header.network.msg.api.DescribeEipAddressesMsg;
import com.gcloud.header.network.msg.api.DescribeEipAddressesReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="DescribeEipAddresses", name = "弹�?�公网IP地址列表")
public class ApiDescribeEipAddressesHandler extends MessageHandler<DescribeEipAddressesMsg, DescribeEipAddressesReplyMsg> {
	@Autowired
	IFloatingIpService eipService;
	
	@Override
	public DescribeEipAddressesReplyMsg handle(DescribeEipAddressesMsg msg) throws GCloudException {
		PageResult<EipAddressSetType> response = eipService.describeEipAddresses(msg);
		DescribeEipAddressesReplyMsg replyMsg = new DescribeEipAddressesReplyMsg();
        replyMsg.init(response);
        return replyMsg;
	}

}