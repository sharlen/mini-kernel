package com.gcloud.controller.security.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityClusterComponentObjectType {

    VM("vm", "云服务器"),
    DOCKER_CONTAINER("dc", "Docker容器")

    ;

    private String value;
    private String cnName;

    SecurityClusterComponentObjectType(String value, String cnName) {
        this.value = value;
        this.cnName = cnName;
    }

    public static SecurityClusterComponentObjectType getByValue(String value){
        return Arrays.stream(SecurityClusterComponentObjectType.values()).filter(t -> t.value().equals(value)).findFirst().orElse(null);
    }

    public String value() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }
}