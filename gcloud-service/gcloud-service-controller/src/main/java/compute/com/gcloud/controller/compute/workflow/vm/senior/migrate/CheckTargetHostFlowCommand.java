package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.model.node.NodeCommentInfo;
import com.gcloud.controller.compute.model.node.ResourceUnit;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.workflow.model.senior.migrate.CheckTargetHostFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.CheckTargetHostFlowCommandRes;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class CheckTargetHostFlowCommand extends BaseWorkFlowCommand{

	@Autowired
	private InstanceDao instanceDao;
	
	@Override
	protected Object process() throws Exception {
		log.debug("CheckTargetHostFlowCommand start");
		CheckTargetHostFlowCommandReq req = (CheckTargetHostFlowCommandReq) getReqParams();
		
		//目标节点�?�?
		Node targetNode = RedisNodesUtil.getComputeNodeByHostName(req.getTargetHostName());
		if(null == targetNode) {
			log.error("::找不到目标节�?");
			throw new GCloudException("::找不到目标节�?");
		}
		String targetNodeIp = targetNode.getNodeIp();
		if(StringUtils.isBlank(targetNodeIp)) {
			log.error("::获取不到目标节点的IP");
			throw new GCloudException("::获取不到目标节点的IP");
		}
		
		//源节点检�?
		VmInstance instance = instanceDao.getById(req.getInstanceId());
		//源节点名称在数据库在初始化的时�?�已经修改了
		Node sourceNode = RedisNodesUtil.getComputeNodeByHostName(req.getSourceHostName());
		if(null == sourceNode) {
			log.error("::找不到源节点");
			throw new GCloudException("::找不到源节点");
		}
		if(sourceNode.getHostName().equals(targetNode.getHostName())) {
			log.error("::不可迁移到同�?个节�?");
			throw new GCloudException("::不可迁移到同�?个节�?");
		}
		if(!sourceNode.getCommentInfo(NodeCommentInfo.HYPERVISOR).equals(targetNode.getCommentInfo(NodeCommentInfo.HYPERVISOR))) {
			log.error("::目标节点和源节点的HYPERVISOR不相�?");
			throw new GCloudException("::目标节点和源节点的HYPERVISOR不相�?");
		}
	
		//目标节点资源情况的检�?
		ResourceUnit ru = RedisNodesUtil.allocateCompute(targetNode.getHostName(), instance.getCore(), instance.getMemory());
		if(null == ru) {
			log.error("::目标节点不可�?");
			allocateRollback(targetNode.getHostName(), instance.getCore(), instance.getMemory());
			throw new GCloudException("::目标节点不可�?");
		}
		if(ru.getCore() < 0) {
			log.error("::CPU核数不足");
			allocateRollback(targetNode.getHostName(), instance.getCore(), instance.getMemory());
			throw new GCloudException("::CPU核数不足");
		}
		if(ru.getMemory() < 0) {
			log.error("::内存容量不足");
			allocateRollback(targetNode.getHostName(), instance.getCore(), instance.getMemory());
			throw new GCloudException("::内存容量不足");
		}
		log.debug("CheckTargetHostFlowCommand end");
		return null;
	}

	//target节点资源不足回滚
	private void allocateRollback(String hostName, int cores, int memory) {
		RedisNodesUtil.releaseCompute(hostName, cores, memory);
	}
	
	@Override
	protected Object rollback() throws Exception {
		System.out.println(this.getClass().getSimpleName() + " rollback()");
		
		//回滚redis分配的资�?
		CheckTargetHostFlowCommandReq req = (CheckTargetHostFlowCommandReq) getReqParams();
		String targetHostName = req.getTargetHostName();
		VmInstance instance = instanceDao.getById(req.getInstanceId());
		
		if(null != instance) {
			allocateRollback(targetHostName, instance.getCore(), instance.getMemory());
		}
		
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CheckTargetHostFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return CheckTargetHostFlowCommandRes.class;
	}

}