package com.gcloud.controller.compute.service.vm.senior;

import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.vm.DescribeMigrateNodesParams;
import com.gcloud.controller.compute.model.vm.VmBundleResponse;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.node.node.model.NodeBaseInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVmSeniorService {

    VmBundleResponse bundle(String instanceId, String imageName, boolean inTask, CurrentUser currentUser);

    void modifyInstanceInit(String instanceId, String instanceType, boolean inTask);
    void joinDomain(String instanceId,String domain);
    VmInstance migrateInit(String instanceId, String targetHost);
    
    PageResult<NodeBaseInfo> describeMigrateNodes(DescribeMigrateNodesParams params, CurrentUser currentUser);
}