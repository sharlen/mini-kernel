package com.gcloud.controller.compute.workflow.vm.network;

import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.compute.workflow.model.network.AttachPortDoneFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.network.AttachPortDoneFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Scope("prototype")
@Slf4j
public class AttachPortDoneFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private IVmBaseService vmBaseService;

    @Override
    protected Object process() throws Exception {
        AttachPortDoneFlowCommandReq req = (AttachPortDoneFlowCommandReq)getReqParams();

        vmBaseService.cleanState(req.getInstanceId(), req.getInTask());

        return null;
    }

    @Override
    protected Object rollback() throws Exception {
        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return AttachPortDoneFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return AttachPortDoneFlowCommandRes.class;
    }
}