/*
 * @Date 2015-4-14
 * 
 * @Author chenyu1@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * @Description �?机参数类
 */
package com.gcloud.controller.compute.model.vm;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StartupParams {
    private List<String> instanceIds;
    private String gTaskId;
    private Boolean isPublicCloud;
    public List<String> getInstanceIds() {
        return instanceIds;
    }

    public void setInstanceIds(List<String> instanceIds) {
        this.instanceIds = instanceIds;
    }

	public String getGTaskId() {
		return gTaskId;
	}

	public void setGTaskId(String gTaskId) {
		this.gTaskId = gTaskId;
	}

    public Boolean getIsPublicCloud() {
        return isPublicCloud;
    }

    public void setIsPublicCloud(Boolean isPublicCloud) {
        this.isPublicCloud = isPublicCloud;
    }

}