package com.gcloud.controller.compute.workflow.model.senior.migrate;

import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateNetcardInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MigrateAttachNetworkWorkflowReq {

	private String instanceId;
	private String targetHostName;
	private String sourceHostName;
	private MigrateNetcardInfo repeatParams;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getTargetHostName() {
		return targetHostName;
	}

	public void setTargetHostName(String targetHostName) {
		this.targetHostName = targetHostName;
	}

	public String getSourceHostName() {
		return sourceHostName;
	}

	public void setSourceHostName(String sourceHostName) {
		this.sourceHostName = sourceHostName;
	}

	public MigrateNetcardInfo getRepeatParams() {
		return repeatParams;
	}

	public void setRepeatParams(MigrateNetcardInfo repeatParams) {
		this.repeatParams = repeatParams;
	}
}