package com.gcloud.controller.compute.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.compute.entity.VmUsb;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Jdbc("controllerJdbcTemplate")
@Repository
public class VmUsbDao extends JdbcBaseDaoImpl<VmUsb, Integer>{
	
	public List<VmUsb> getVmUsbByInsIds(List<String> instanceIds) {
		
		String inPreSql = SqlUtil.inPreStr(instanceIds.size());
    	String sql = "select * from gc_vm_usb where instanceId in (" + inPreSql + ")";
    	List<Object> values = new ArrayList<Object>();
    	values.addAll(instanceIds);
    	
    	return this.findBySql(sql, values);
	}
	
}