package com.gcloud.controller.compute.handler.api.vm.senior;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.senior.bundle.BundleInstanceWorkflow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.senior.ApiCreateImageMsg;
import com.gcloud.header.compute.msg.api.vm.senior.ApiCreateImageReplyMsg;
import com.gcloud.header.log.model.Task;

//import com.gcloud.controller.enums.ResourceIsolationCheckType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule=SubModule.VM, action = "CreateImage")
@LongTask
@GcLog(isMultiLog = true, taskExpect = "云服务器打包")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiCreateImageHandler extends BaseWorkFlowHandler<ApiCreateImageMsg, ApiCreateImageReplyMsg> {

    @Override
    public Object preProcess(ApiCreateImageMsg msg) throws GCloudException {
        return null;
    }

    @Override
    public ApiCreateImageReplyMsg process(ApiCreateImageMsg msg) throws GCloudException {

        ApiCreateImageReplyMsg replyMessage = new ApiCreateImageReplyMsg();
        BundleInstanceInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), BundleInstanceInitFlowCommandRes.class);
        replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).objectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId())).expect("云服务器打包成功").build());
        replyMessage.setImageId(res.getImageId());
        replyMessage.setImageName(res.getImageName());

        return replyMessage;
    }

    @Override
    public Object initParams(ApiCreateImageMsg msg) {
        BundleInstanceWorkflowReq req = new BundleInstanceWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setImageName(msg.getImageName());
        req.setInTask(false);
        req.setCurrentUser(msg.getCurrentUser());
        return req;
    }

    @Override
    public Class getWorkflowClass() {
        return BundleInstanceWorkflow.class;
    }
}