package com.gcloud.controller.compute.service.node.impl;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.dao.ComputeNodeDao;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.ZoneDao;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.entity.ComputeNode;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.AttachNodeParams;
import com.gcloud.controller.compute.model.node.AvailableVmResource;
import com.gcloud.controller.compute.model.node.DescribeNodesParams;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.model.node.NodeCommentInfo;
import com.gcloud.controller.compute.service.node.IComputeNodeService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.utils.VmControllerUtil;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.StoragePoolZoneDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.StoragePoolZone;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.ComputeNodeState;
import com.gcloud.header.compute.msg.api.model.NodeResource;
import com.gcloud.header.compute.msg.node.node.model.ComputeNodeInfo;
import com.gcloud.header.compute.msg.node.node.model.NodeBaseInfo;
import com.gcloud.service.common.Consts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Slf4j
public class ComputeNodeServiceImpl implements IComputeNodeService {

    @Autowired
    private GCloudRedisTemplate redisTemplate;

    @Autowired
    private ControllerComputeProp prop;

    @Autowired
    private ComputeNodeDao nodeDao;
    
    @Autowired
    private ZoneDao zoneDao;

    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private VolumeDao volumeDao;
    
    @Autowired
    private StoragePoolDao poolDao;
    
    @Autowired
    private StoragePoolZoneDao poolZoneDao;

    @Override
    public void computeNodeConnect(ComputeNodeInfo computeNodeInfo, int nodeTimeout) {

        String hostname = computeNodeInfo.getHostname();

        String aliveKey = VmControllerUtil.getNodesAliveKey(hostname);
        String aliveNode = ObjectUtils.toString(redisTemplate.opsForValue().get(aliveKey));
        if (StringUtils.isBlank(aliveNode)) {
            log.debug("node has gone away, node=" + hostname);
            //判断是否能注�?, 证书控制，先注释，后面等待控制开发完�?
            //TODO
//            if(ControllerSystemContext.getProp().isCpuScaleLimit() && !ControllerRestApi.canNodeRegist(hostname, computeNodeInfo.getPhysicalCpu())) {
//                return;
//            }

            Node node = new Node();
            node.getAvailableVmResource().setAvailableCore(computeNodeInfo.getMaxCore());
            node.getAvailableVmResource().setAvailableMemory(computeNodeInfo.getMaxMemory());
            node.getAvailableVmResource().setAvailableDisk(computeNodeInfo.getMaxDisk());

            node.getAvailableVmResource().setMaxCore(computeNodeInfo.getMaxCore());
            node.getAvailableVmResource().setMaxMemory(computeNodeInfo.getMaxMemory());
            node.getAvailableVmResource().setPhysicalCpu(computeNodeInfo.getPhysicalCpu());

            BigDecimal b = new BigDecimal(computeNodeInfo.getMaxDisk());
            double maxDisk = b.setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue();
            node.getAvailableVmResource().setMaxDisk(maxDisk);

            node.setNodeIp(computeNodeInfo.getNodeIp());
            node.setHostName(hostname);
            node.setCommentInfo(NodeCommentInfo.HYPERVISOR, computeNodeInfo.getHypervisor());
            node.setCommentInfo(NodeCommentInfo.KERNEL_VERSION, computeNodeInfo.getKernelVersion());
            node.setCommentInfo(NodeCommentInfo.CPU_TYPE, computeNodeInfo.getCpuType());
            node.setCommentInfo(NodeCommentInfo.CLOUD_PLATFORM, computeNodeInfo.getCloudPlatform());
            node.setCommentInfo(NodeCommentInfo.IS_FT, String.valueOf(computeNodeInfo.getIsFt()));
            node.setUpdateTime(new Date());

            ComputeNode nodeEntity = this.nodeDao.findUniqueByProperty("hostname", hostname);
            if (nodeEntity != null) {
                node.setZoneId(nodeEntity.getZoneId());
            }
            
            RedisNodesUtil.registerComputeNode(hostname, node);
            RedisNodesUtil.updateNodeAlive(hostname, nodeTimeout);
        }
        else {
            // 判断node是否存在redis里面
            // 更新节点信息
            updateNodeInfo(hostname, nodeTimeout);
        }

    }

    private void updateNodeInfo(String hostname, int nodeTimeout) {

        String nodeKey = VmControllerUtil.getNodesHostNameKey(hostname);
        String aliveKey = VmControllerUtil.getNodesAliveKey(hostname);

        String lockName = VmControllerUtil.getNodesHostNameLock(hostname);
        String lockid = "";
        Node node = null;
        try {
            lockid = LockUtil.spinLock(lockName, Consts.Time.NODES_REDIS_LOCK_TIMEOUT, Consts.Time.NODES_REDIS_LOCK_GET_LOCK_TIMEOUT);
            node = (Node)redisTemplate.getObject(nodeKey);
            if (node == null) {
                // 如果不存在，直接删除aliveNode，等待下次汇�?
                redisTemplate.delete(aliveKey);
            }
            else {
                log.debug(String.format("Update the compute node:%s", hostname));
                redisTemplate.setObject(nodeKey, node);
            }

        }
        catch (Exception ex) {
            log.error("节点更新信息失败:获取锁失�?,hostName=" + hostname, ex);
        }
        finally {
            LockUtil.releaseSpinLock(lockName, lockid);
        }

        if (node != null) {
            // 更新节点updateTime
            RedisNodesUtil.updateNodeAlive(hostname, nodeTimeout);
        }

    }

	@Override
	public PageResult<NodeBaseInfo> describeNodes(DescribeNodesParams params, CurrentUser currentUser) {
        if(params.getState() != null){
            ComputeNodeState state = ComputeNodeState.value(params.getState());
            if(state == null){
                throw new GCloudException("::不支持此状�??");
            }
        }

        PageResult<NodeBaseInfo> page = nodeDao.page(params, NodeBaseInfo.class);

        if(page != null && page.getList() != null && page.getList().size() > 0){

            Map<Integer, String> states = new HashMap<>();
            Arrays.stream(ComputeNodeState.values()).forEach(s -> states.put(s.getValue(), s.getCnName()));

            for(NodeBaseInfo nodeInfo : page.getList()){

                nodeInfo.setCnState(states.get(nodeInfo.getState()));

                //不显示资�?
                if(params.getResource() != null && params.getResource()){
                    Node node = RedisNodesUtil.getComputeNodeByHostName(nodeInfo.getHostName());
                    if(node != null){
                        AvailableVmResource resource = node.getAvailableVmResource();
                        nodeInfo.setCpuTotal(resource.getMaxCore());
                        nodeInfo.setCpuAvail(resource.getAvailableCore());
                        nodeInfo.setMemoryTotal(resource.getMaxMemory());
                        nodeInfo.setMemoryAvail(resource.getAvailableMemory());
                    }
                }
            }
        }

		return page;
	}

	@Override
	public void attachNode(AttachNodeParams params, CurrentUser currentUser) {

        ComputeNode computeNode = nodeDao.findUniqueByProperty(ComputeNode.HOSTNAME, params.getHostname());
        if(computeNode == null){
            log.error("1020204::未找到该节点");
            throw new GCloudException("1020204::未找到该节点");
        }

        if(StringUtils.isNotBlank(computeNode.getZoneId())){
            throw new GCloudException("1020203::已经关联了可用区");
        }

        AvailableZoneEntity zone = zoneDao.getById(params.getZoneId());
        if(zone == null){
            log.error("1020203::未找到该可用�?");
            throw new GCloudException("1020203::未找到该可用�?");
        }

        int result = nodeDao.allocateZone(computeNode.getHostname(), zone.getId());
        if(result == 0){
            throw new GCloudException("1020203::已经关联了可用区");
        }
        
        //节点关联了可用区,更新已有的存储池可用区表
        poolZoneDao.updateZoneByPoolHostname(computeNode.getHostname(), zone.getId());
        
        //添加没有关联可用区的存储�?
        List<StoragePool> poolList = poolDao.findPoolWithoutZoneByHostname(computeNode.getHostname());
        if(null != poolList && !poolList.isEmpty()) {
        	List<StoragePoolZone> list = new ArrayList<>();
        	for (StoragePool pool : poolList) {
        		StoragePoolZone tmp = new StoragePoolZone();
        		tmp.setStoragePoolId(pool.getId());
        		tmp.setZoneId(zone.getId());
        		list.add(tmp);
			}
        	poolZoneDao.saveBatch(list);
        }

        RedisNodesUtil.updateComputeNodeZone(computeNode.getHostname(), zone.getId());

	}

    @Override
    public NodeResource nodeResource() {
        NodeResource resourceRes = new NodeResource();
        Map<String, Node> nodeMap = RedisNodesUtil.getComputeNodes();
        for (String key : nodeMap.keySet()) {
            AvailableVmResource avr = nodeMap.get(key).getAvailableVmResource();
            if (avr != null) {
                resourceRes.addCpuTotal(avr.getMaxCore());
                resourceRes.addCpuAvail(avr.getAvailableCore());
                resourceRes.addMemoryTotal(avr.getMaxMemory());
                resourceRes.addMemoryAvail(avr.getAvailableMemory());
            }
        }

        return resourceRes;
    }

    @Override
    public void detachNode(String hostname){

        ComputeNode computeNode = nodeDao.findUniqueByProperty(ComputeNode.HOSTNAME, hostname);
        if(computeNode == null){
            throw new GCloudException("::找不到节�?");
        }
        if(StringUtils.isBlank(computeNode.getZoneId())){
            throw new GCloudException("::没有关联可用�?");
        }

        VmInstance vmInstance = instanceDao.findOneByProperty(VmInstance.HOSTNAME, hostname);
        if(vmInstance != null){
            throw new GCloudException("::节点上有关联的云服务器，不能取消可用�?");
        }

        if(volumeDao.hasLocalVolume(hostname)){
            throw new GCloudException("::节点上有关联的磁盘，不能取消可用�?");
        }

        List<String> updateFiled = new ArrayList<>();
        updateFiled.add(ComputeNode.ZONE_ID);
        computeNode.setZoneId(null);
        nodeDao.update(computeNode, updateFiled);
        
        //节点取消关联可用区的时�?�，相应节点的存储池也取消关�?
        poolZoneDao.deleteByHostname(computeNode.getHostname());

        RedisNodesUtil.updateComputeNodeZone(hostname, null);

    }
}