package com.gcloud.controller.compute.handler.node.node;

import com.gcloud.controller.compute.service.node.IComputeNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.msg.node.node.ComputeNodeConnectMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Handler
@Slf4j
public class ComputeNodeConnectHandler extends AsyncMessageHandler<ComputeNodeConnectMsg> {

    @Autowired
    private IComputeNodeService computeNodeService;

    @Override
    public void handle(ComputeNodeConnectMsg msg) {
        log.debug("ComputeNodeConnectHandler begin, node=" + msg.getComputeNodeInfo().getHostname());
        computeNodeService.computeNodeConnect(msg.getComputeNodeInfo(), msg.getNodeTimeout());
        log.debug("ComputeNodeConnectHandler end, node=" + msg.getComputeNodeInfo().getHostname());
    }
}