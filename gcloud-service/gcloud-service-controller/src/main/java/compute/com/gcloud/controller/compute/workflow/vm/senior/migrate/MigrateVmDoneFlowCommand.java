package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmDoneFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmDoneFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.VmState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class MigrateVmDoneFlowCommand extends BaseWorkFlowCommand{

	@Autowired
	private InstanceDao instanceDao;

	@Override
	protected Object process() throws Exception {

		MigrateVmDoneFlowCommandReq req = (MigrateVmDoneFlowCommandReq)getReqParams();

		VmInstance vmInstance = instanceDao.getById(req.getInstanceId());

		if(VmState.RUNNING.value().equals(req.getBeginStatus())){
			try{

				RedisNodesUtil.releaseCompute(req.getSourceHostName(), vmInstance.getCore(), vmInstance.getMemory());
			}catch (Exception ex){
				log.error("::迁移释放原节点资源失�?", ex);
			}
		}


		List<String> updateFiled = new ArrayList<>();

		vmInstance.setStepState(null);
		updateFiled.add(VmInstance.STEP_STATE);
		vmInstance.setTaskState(null);
		updateFiled.add(VmInstance.TASK_STATE);
		vmInstance.setHostname(req.getTargetHostName());
		updateFiled.add(VmInstance.HOSTNAME);
		vmInstance.setMigrateTo(null);
		updateFiled.add(VmInstance.MIGRATE_TO);

		instanceDao.update(vmInstance, updateFiled);


		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		

		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return MigrateVmDoneFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return MigrateVmDoneFlowCommandRes.class;
	}

}