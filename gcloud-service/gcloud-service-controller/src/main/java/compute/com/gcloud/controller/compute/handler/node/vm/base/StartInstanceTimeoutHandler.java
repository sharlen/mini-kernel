package com.gcloud.controller.compute.handler.node.vm.base;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageTimeoutHandler;
import com.gcloud.header.compute.msg.node.vm.base.StartInstanceMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Handler
@Slf4j
public class StartInstanceTimeoutHandler extends MessageTimeoutHandler<StartInstanceMsg> {

    @Autowired
    private InstanceDao vmInstanceDao;

    @Override
    public void timeout(StartInstanceMsg msg) {
        VmInstance vm = vmInstanceDao.getById(msg.getInstanceId());
        vmInstanceDao.cleanState(vm.getId());
        try{
            Dispatcher.dispatcher().release(vm.getHostname(), vm.getCore(), vm.getMemory());
        }catch (Exception ex){
            log.error("�?机失败，释放资源失败", ex);
        }
    }
}