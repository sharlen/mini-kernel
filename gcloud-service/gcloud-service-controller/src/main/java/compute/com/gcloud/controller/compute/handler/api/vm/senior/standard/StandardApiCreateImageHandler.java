package com.gcloud.controller.compute.handler.api.vm.senior.standard;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.senior.BundleInstanceWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.senior.bundle.BundleInstanceWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.senior.standard.StandardApiCreateImageMsg;
import com.gcloud.header.compute.msg.api.vm.senior.standard.StandardApiCreateImageReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule=SubModule.VM, action = "CreateImage", versions = {ApiVersion.Standard})
@LongTask
@GcLog(isMultiLog = true, taskExpect = "云服务器打包")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class StandardApiCreateImageHandler extends BaseWorkFlowHandler<StandardApiCreateImageMsg, StandardApiCreateImageReplyMsg>{

	@Override
	public Object preProcess(StandardApiCreateImageMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public StandardApiCreateImageReplyMsg process(StandardApiCreateImageMsg msg) throws GCloudException {
		StandardApiCreateImageReplyMsg replyMessage = new StandardApiCreateImageReplyMsg();
        BundleInstanceInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), BundleInstanceInitFlowCommandRes.class);
        msg.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).expect("云服务器打包成功").build());
        replyMessage.setImageId(res.getImageId());
        
        return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return BundleInstanceWorkflow.class;
	}
	
	@Override
    public Object initParams(StandardApiCreateImageMsg msg) {
        BundleInstanceWorkflowReq req = new BundleInstanceWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setImageName(msg.getImageName());
        req.setInTask(false);
        req.setCurrentUser(msg.getCurrentUser());
        return req;
    }

}