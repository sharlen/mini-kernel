package com.gcloud.controller.utils;

import com.gcloud.common.model.PageParams;
import com.gcloud.framework.db.PageResult;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiUtil {

    public static <T> PageResult<T> toPage(PageResult<?> page, List<T> datas){

        PageResult<T> pageResult = new PageResult<>();
        pageResult.setList(datas);
        pageResult.setPageNo(page.getPageNo());
        pageResult.setPageSize(page.getPageSize());
        pageResult.setTotalCount(page.getTotalCount());

        return pageResult;
    }

//    public static <T> PageResult<T> emptyPage(T clazz, PageParams pageParams){
//
//        PageResult<T> pageResult = new PageResult<>();
//        List<T> datas = new ArrayList<>();
//        pageResult.setList(datas);
//        pageResult.setPageNo(pageParams.getPageNumber());
//        pageResult.setPageSize(pageParams.getPageSize());
//        pageResult.setTotalCount(0);
//
//        return pageResult;
//
//    }

    public static PageResult emptyPage(PageParams pageParams){

        PageResult pageResult = new PageResult<>();
        List datas = new ArrayList<>();
        pageResult.setList(datas);
        pageResult.setPageNo(pageParams.getPageNumber());
        pageResult.setPageSize(pageParams.getPageSize());
        pageResult.setTotalCount(0);

        return pageResult;

    }
}