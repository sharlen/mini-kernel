package com.gcloud.controller.timer;

import com.gcloud.controller.dao.MessageTaskDao;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@QuartzTimer(cron = "0 0 1 * * ? ") 
@Slf4j
public class MessageTaskCleanTimer extends GcloudJobBean {

    @Autowired
    private MessageTaskDao messageTaskDao;

    private final int DEFAULT_FINISH_RETENTION = 2;
    private final int DEFAULT_RETENTION = 3;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        messageTaskDao.deleteFinishTask(DEFAULT_FINISH_RETENTION);
        messageTaskDao.deleteTask(DEFAULT_RETENTION);

    }
}