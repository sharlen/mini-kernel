package com.gcloud.controller.utils;

import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.identity.IApiIdentity;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class UserUtil {

    /**
     *
     * @return 返回用户，找不到或�?�不存在，返回null
     */
    public static GetUserReplyMsg user(String userId){
        GetUserReplyMsg user = null;
        try{
            IApiIdentity apiIdentity = SpringUtil.getBean(IApiIdentity.class);
            user = apiIdentity.getUserById(userId);
        }catch (Exception ex){
            log.error(String.format("获取用户失败, userId:%s, ex:%s", userId, ex), ex);
        }
        return user;
    }

    public static String userName(String userId){
        return CacheContainer.getInstance().getString(CacheType.USER_NAME, userId);
    }
    
    public static GetLdapConfReplyMsg getLdapConf(String domain) {
    	GetLdapConfReplyMsg reply=null;
    	try {
    		 IApiIdentity apiIdentity = SpringUtil.getBean(IApiIdentity.class);
    		 reply=apiIdentity.getLdapConf(domain);
    	}catch (Exception ex){
            log.error(String.format("获取域服务失�?, domainId:%s, ex:%s", domain, ex), ex);
        }
    	return reply;
    }
}