package com.gcloud.controller.log.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.log.entity.Log;
import com.gcloud.core.currentUser.enums.RoleType;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.log.msg.api.ApiDescribeLogsMsg;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
@Slf4j
public class LogDao extends JdbcBaseDaoImpl<Log, Long> {
	public Log findTask(String taskId,String objectId) {
		StringBuffer sql = new StringBuffer();
		List<Object> parameterValues = new ArrayList<Object>();
		sql.append("select * from gc_log where task_id = ?");
		parameterValues.add(taskId);
		if(null != objectId) {
			sql.append(" and object_id=?");
			parameterValues.add(objectId);
		}
		List<Log> logs = findBySql(sql.toString(), parameterValues);
		if(logs != null && logs.size() > 0)
			return logs.get(0);
		return null;
	}
	
	public <E> PageResult<E>  describeLogs(ApiDescribeLogsMsg msg,  Class<E> clazz) {
		
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();

		sql.append("select l.* from gc_log l where l.id is not null ");
		
		if (!msg.getCurrentUser().getRole().equals(RoleType.SUPER_ADMIN.getRoleId())) {
			sql.append(" and user_name = ?");
			values.add(msg.getCurrentUser().getLoginName());
		}
		
		if(msg.isOnlyLongTask()) {
			sql.append(" and timeout is not null ");
		}
		
		if(StringUtils.isNotBlank(msg.getKey())){
			sql.append(" and (user_name LIKE ?");
			values.add("%" + msg.getKey() + "%");
			sql.append(" or fun_name LIKE ?)");
			values.add("%" + msg.getKey() +"%" );
		}
		
		if(StringUtils.isNotBlank(msg.getUserName())){
			sql.append(" and user_name LIKE ?");
			values.add("%" + msg.getUserName() + "%");
		}
		
		if(StringUtils.isNotBlank(msg.getFunName())){
			sql.append(" and fun_name LIKE ?");
			values.add("%" + msg.getFunName() +"%" );
		}
		
		if(StringUtils.isNotBlank(msg.getStartTime())) {
			sql.append(" and start_time >= ?");
			values.add(msg.getStartTime());
		}
		
		if(StringUtils.isNotBlank(msg.getEndTime())) {
			sql.append(" and start_time <= ?");
			values.add(msg.getEndTime());
		}
		
		//根据状�?�过�?
		if(null != msg.getResult()) {
			sql.append(" and result = ?");
			values.add(msg.getResult().byteValue());
		}
		
		
		sql.append(" order by start_time desc");

		return findBySql(sql.toString(), values, msg.getPageNumber(), msg.getPageSize(), clazz);
	}
	
	/**
	 * @param logKept 日志记录保留有效时间
	 * @param fileLogKept 归档保留有效时间；为-1时表示不归档
	 */
	public void fileLog(int logKeptDays, int fileLogKeptDays) {
		try {
			jdbcTemplate.execute("call file_log(" + logKeptDays + ", " + fileLogKeptDays + ")"); 
		} catch(Exception ex) {
			log.error("调用日志归档存储过程失败" , ex);
		}
	}
}