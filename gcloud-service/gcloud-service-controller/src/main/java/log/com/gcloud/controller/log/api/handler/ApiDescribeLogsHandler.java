package com.gcloud.controller.log.api.handler;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.log.service.ILogService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.log.model.LogAttributesType;
import com.gcloud.header.log.msg.api.ApiDescribeLogsMsg;
import com.gcloud.header.log.msg.api.ApiDescribeLogsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.LOG,action = "DescribeLogs",name="日志列表")
public class ApiDescribeLogsHandler extends MessageHandler<ApiDescribeLogsMsg, ApiDescribeLogsReplyMsg>{
	@Autowired
	private ILogService logService;
	
	@Override
	public ApiDescribeLogsReplyMsg handle(ApiDescribeLogsMsg msg) throws GCloudException {
		PageResult<LogAttributesType> response = logService.describeLogs(msg);
		ApiDescribeLogsReplyMsg replyMsg = new ApiDescribeLogsReplyMsg();
		replyMsg.init(response);
		return replyMsg;
	}
}