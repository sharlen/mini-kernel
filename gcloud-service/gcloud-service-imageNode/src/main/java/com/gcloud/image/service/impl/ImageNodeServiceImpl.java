package com.gcloud.image.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.image.msg.node.DeleteImageMsg;
import com.gcloud.header.image.msg.node.DeleteImageReplyMsg;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
import com.gcloud.header.image.msg.node.DownloadImageReplyMsg;
import com.gcloud.image.driver.ImageCacheDriverEnum;
import com.gcloud.image.prop.ImageNodeProp;
import com.gcloud.image.provider.enums.ImageProviderEnum;
import com.gcloud.image.service.IImageNodeService;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Slf4j
public class ImageNodeServiceImpl implements IImageNodeService {
	@Autowired
	ImageNodeProp prop;
	
	@Autowired
	private MessageBus bus;

	@Override
	public void downloadImage(DownloadImageMsg msg) {
		
		DownloadImageReplyMsg reply = new DownloadImageReplyMsg();
		reply.setServiceId(MessageUtil.controllerServiceId());
		reply.setSuccess(true);
		reply.setTaskId(msg.getTaskId());
		reply.setImageId(msg.getImageId());
		reply.setStoreTarget(msg.getTarget());
		reply.setImageResourceType(msg.getImageResourceType());
		
		try {
			ImageProviderEnum.getByType(msg.getProvider()).downloadImage(msg);
		} catch (Exception e) {
			log.error("ImageServiceImpl downloadImage error", e);
			reply.setSuccess(false);
			reply.setErrorCode(ErrorCodeUtil.getErrorCode(e, "::下载镜像异常"));
		}
		
		bus.send(reply);
	}

	@Override
	public void deleteImage(DeleteImageMsg msg) {
		DeleteImageReplyMsg reply = new DeleteImageReplyMsg();
		reply.setServiceId(MessageUtil.controllerServiceId());
		reply.setSuccess(true);
		reply.setTaskId(msg.getTaskId());
		reply.setImageId(msg.getImageId());
		reply.setStoreTarget(msg.getStoreTarget());
		try {
			ImageCacheDriverEnum.getByType(msg.getStoreType()).deleteImageCache(msg.getImageId(), msg.getStoreTarget(), msg.getResourceType());
		} catch (Exception e) {
			log.error("ImageServiceImpl deleteImage error", e);
			reply.setSuccess(false);
			reply.setErrorCode(ErrorCodeUtil.getErrorCode(e, "::删除镜像缓存异常"));
		}
		
		bus.send(reply);
	}

}