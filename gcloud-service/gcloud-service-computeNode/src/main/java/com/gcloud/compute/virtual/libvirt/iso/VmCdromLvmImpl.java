package com.gcloud.compute.virtual.libvirt.iso;

import org.dom4j.Document;
import org.dom4j.Element;
import org.springframework.stereotype.Component;

import com.gcloud.header.compute.enums.DiskProtocol;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component("VmCdromLvmImpl")
public class VmCdromLvmImpl implements IVmCdrom{

	@Override
	public void addCdromDevice(Document doc, VmCdromDetail vmcdromDetail, boolean isCreateElement) {
		Element diskE = null;
		if (!isCreateElement) {
			Element devices = (Element) doc.selectSingleNode("/domain/devices");
			diskE = devices.addElement("disk");
		} else {
			diskE = doc.addElement("disk");
		}
		
		diskE.addAttribute("device", "cdrom");
		diskE.addAttribute("type", "block");

		Element driverE = diskE.addElement("driver");
		driverE.addAttribute("name", "qemu");
		driverE.addAttribute("type", "qcow2");

		Element sourceE = diskE.addElement("source");
		sourceE.addAttribute("dev", vmcdromDetail.getIsoPath());

		Element targetE = diskE.addElement("target");
		targetE.addAttribute("dev", vmcdromDetail.getIsoTargetDevice());
		targetE.addAttribute("bus", "ide");
	}

	@Override
	public DiskProtocol disProtocol() {
		return DiskProtocol.LVM;
	}

}