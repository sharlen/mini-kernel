package com.gcloud.compute.service.vm.senior.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.service.vm.senior.IVmSeniorNodeService;
import com.gcloud.compute.util.ConfigFileUtil;
import com.gcloud.compute.util.VmNodeUtil;
import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.core.condition.ConditionalHypervisor;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.header.compute.enums.DiskProtocol;
import com.gcloud.header.compute.enums.FileFormat;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.service.common.compute.model.DomainInfo;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.compute.uitls.DiskUtil;
import com.gcloud.service.common.compute.uitls.VmUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ConditionalHypervisor
@Slf4j
public class VmSeniorNodeServiceKvmImpl implements IVmSeniorNodeService {

    @Autowired
    private ComputeNodeProp prop;
    
    @Autowired
	private IVmVirtual vmVirtual;

    private final FileFormat DEFAULT_FORMAT = FileFormat.QCOW2;

    @Override
    public void convertToImage(String instanceId, FileFormat targetFormat, VmVolumeDetail volumeDetail) {

        DomainInfo domInfo = VmNodeUtil.checkVm(instanceId);
        if (domInfo == null) {
            throw new GCloudException("1011202::云服务器不存�?");
        }

        //不是关机, 则抛�?
        if(!VmState.STOPPED.value().equals(domInfo.getGcState())){
            throw new GCloudException("1011205::云服务器�?要为关机状�??");
        }

        String targetPath = VmUtil.getBundleTargetPath(VmNodeUtil.getInstanceConfigPath(prop.getNodeIp(), instanceId));

        String systemDiskPath = volumeDetail.getSourcePath();
        String systemDiskType = volumeDetail.getFileFormat();
        String systemDiskProtocol = volumeDetail.getProtocol();

        if(StringUtils.isBlank(systemDiskPath)){
            throw new GCloudException("1011203::找不到系统盘");
        }

        String convertSource = DiskUtil.getDiskPath(systemDiskPath, DiskProtocol.value(systemDiskProtocol));

        if(StringUtils.isBlank(systemDiskType)){
            QemuInfo qemuInfo = DiskQemuImgUtil.info(convertSource);
            systemDiskType = qemuInfo.getFormat();
        }

        FileFormat targetImageFormat = targetFormat == null ? DEFAULT_FORMAT : targetFormat;

        boolean isCompress = targetImageFormat.equals(FileFormat.QCOW2);

        try{
            DiskQemuImgUtil.convert(convertSource, targetPath, systemDiskType, targetImageFormat.getValue(), isCompress);
        }catch (Exception ex){
            File file = new File(targetPath);
            if(file.exists()){
                file.delete();
            }
            throw ex;
        }


    }

    @Override
    public void deleteBundle(String instanceId, String nodeIp) {

        nodeIp = StringUtils.isBlank(nodeIp) ? prop.getNodeIp() : nodeIp;


        String targetPath = VmUtil.getBundleTargetPath(VmNodeUtil.getInstanceConfigPath(nodeIp, instanceId));

        File file = new File(targetPath);
        if(file.exists()){
            file.delete();
        }
    }

	@Override
	public void migrate(String type, String migrateProtocol, String instanceId, String targetIp, String targetHostName, Boolean isStorageAll) {
		if(StringUtils.equalsIgnoreCase("TCP", migrateProtocol)) {
			VmUtil.migrate(type, instanceId, targetIp, isStorageAll);
		} else if(StringUtils.equalsIgnoreCase("TLS", migrateProtocol)) {
			VmUtil.tlsMigrate(type, instanceId, targetHostName, isStorageAll);
		}
	}


    public void migrateInstance(String type, String migrateProtocol, String instanceId, String sourceIp, String targetIp, String targetHostName, Boolean isStorageAll) {
        DomainInfo domInfo = VmNodeUtil.checkVm(instanceId);
        if (domInfo == null) {
            throw new GCloudException("::找不到云服务�?");// 没找到该云服务器
        }
        String state = domInfo.getGcState();

        if(!VmState.RUNNING.value().equals(state) && !VmState.STOPPED.value().equals(state)){
            throw new GCloudException("::云服务器当前状�?�不支持迁移");
        }


        SimpleFlowChain<String, String> chain = new SimpleFlowChain("vm migration");

        chain.then(new Flow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                ConfigFileUtil.updateNodeIpWithCreateLock(instanceId, sourceIp, targetIp);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, String data) {
                ConfigFileUtil.updateNodeIpWithCreateLock(instanceId, targetIp, sourceIp);
                chain.rollback();
            }
        }).then(new NoRollbackFlow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                //迁移不会滚，迁移后的操作不能影响虚拟机正常使�?
                if(VmState.RUNNING.value().equals(state)){
                    migrate("live", migrateProtocol, instanceId, targetIp, targetHostName, isStorageAll);
                }

                chain.next();

            }

        }).then(new NoRollbackFlow<String>() {
            @Override
            public void run(SimpleFlowChain chain, String data) {
                String libXmlPath = VmNodeUtil.getLibvirtXmlPath(targetIp, instanceId);
                vmVirtual.define(libXmlPath, targetIp);
                chain.next();
            }

        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }
    }
}