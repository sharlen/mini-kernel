package com.gcloud.compute.handler.vm.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.compute.service.vm.iso.IVmIsoNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.iso.CleanIsoConfigFileMsg;
import com.gcloud.header.compute.msg.node.vm.iso.CleanIsoConfigFileReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class CleanIsoConfigFileHandler extends AsyncMessageHandler<CleanIsoConfigFileMsg>{
	@Autowired
	private IVmIsoNodeService vmIsoService;

	@Autowired
	private MessageBus bus;
	
	@Override
	public void handle(CleanIsoConfigFileMsg msg) {
		CleanIsoConfigFileReplyMsg replyMsg = msg.deriveMsg(CleanIsoConfigFileReplyMsg.class);
		replyMsg.setInstanceId(msg.getInstanceId());
		replyMsg.setIsoId(msg.getVmCdromDetail().getIsoId());
		replyMsg.setSuccess(false);
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		try {
			vmIsoService.cleanIsoFile(msg.getInstanceId(), msg.getVmCdromDetail());
			replyMsg.setSuccess(true);
		} catch (Exception ex) {
			log.error("删除光盘文件错误", ex);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::删除光盘文件错误"));
		}
		bus.send(replyMsg);
		
	}

}