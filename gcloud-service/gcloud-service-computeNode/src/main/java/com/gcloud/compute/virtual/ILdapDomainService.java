package com.gcloud.compute.virtual;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface ILdapDomainService {
	boolean join(String instanceId,String domain,String user,String password);
	boolean remove(String instanceId,String domain,String user,String password);
	boolean move(String instanceId,String sourceDomain,String sourceUser,String sourcePassword,String targetDomain,String targetUser,String targetPassword);
}