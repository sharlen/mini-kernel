package com.gcloud.api.filter;

import com.gcloud.api.ApiIdentityConfig;
import com.gcloud.common.constants.HttpRequestConstant;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.identity.IApiIdentity;
import com.gcloud.core.identity.TokenUser;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.identity.enums.UncheckApiEnum;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;
import com.gcloud.header.identity.user.model.GetUserModel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class AuthFilter implements Filter{
	private static List<String> excludedUrls = null;//Arrays.asList("/user/login", "/authcode/kaptcha", "/apidoc", "/image/download", "/image/upload", "/ecs/DescribeRegions");
	@Autowired
	ApiIdentityConfig identityConfig;
	@Autowired
	IApiIdentity apiIdentity;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		log.info("---------------------�?始进入请求地�?拦截----------------------------"); 
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String reqPath = httpRequest.getRequestURI();
		log.debug(String.format("【认证拦截器】拦截地�?�?%s", reqPath));
		
		List<String> uncheckApis = (List<String>)CacheContainer.getInstance().get(CacheType.AUTH_UNCHECK_API.name());
		if(uncheckApis == null) {
			DescribeUncheckApiReplyMsg reply = apiIdentity.getUncheckApi(UncheckApiEnum.AUTH.name());
			uncheckApis = reply.getResponse().getPath();
			CacheContainer.getInstance().put(CacheType.AUTH_UNCHECK_API.name(), uncheckApis, identityConfig.getTokenTimeout());
		}
		excludedUrls = uncheckApis;
		if(uncheckApis.contains(reqPath)) {
			chain.doFilter(request, response);
			return ;
		}
		
		String tokenId = httpRequest.getHeader(HttpRequestConstant.HEADER_TOKEN_ID);
		if(null != tokenId) {
			/*TokenUser tokenUser=(TokenUser) CacheContainer.getInstance().get(CacheType.TOKEN_USER, tokenId);
			if(tokenUser==null)
				tokenUser=apiIdentity.checkToken(tokenId);*/
			TokenUser tokenUser = apiIdentity.checkToken(tokenId);
			if(tokenUser!=null && tokenUser.getExpressTime() > System.currentTimeMillis()) {
				tokenUser.setExpressTime(System.currentTimeMillis() + identityConfig.getTokenTimeout() * 1000);
				//CacheContainer.getInstance().put(CacheType.TOKEN_USER, tokenId, tokenUser);
				
				CacheContainer.getInstance().refresh(tokenId, identityConfig.getTokenTimeout());
				CacheContainer.getInstance().refresh(tokenUser.getUserInfo().getLoginName(), identityConfig.getTokenTimeout());
				
				httpRequest.setAttribute(HttpRequestConstant.ATTR_USER_INFO, tokenUser.getUserInfo());
	        	chain.doFilter(httpRequest, response);
	        } else {  
	        	//CacheContainer.getInstance().remove(CacheType.TOKEN_USER, tokenId);
	        	throw new GCloudException("C003_MNG_0001::会话无效，请登录");
	        }
		} else if(identityConfig.isSignatureCheck()) {
			chain.doFilter(request, response);
			return;
		}else if(identityConfig.isTestMode()) {
			GetUserReplyMsg userReply = apiIdentity.getUserById(identityConfig.getTestUserId());
			GetUserModel userInfo = BeanUtil.copyProperties(userReply, GetUserModel.class);
			httpRequest.setAttribute(HttpRequestConstant.ATTR_USER_INFO, userInfo);
			
			chain.doFilter(request, response);
			return;
		}  else {
			chain.doFilter(request, response);
			return;
			//throw new GCloudException("C003_MNG_0002::会话无效，请登录");
		}
	}

	@Override
	public void destroy() {
		
	}

	public static List<String> getExcludedUrls() {
		return excludedUrls;
	}
	
}