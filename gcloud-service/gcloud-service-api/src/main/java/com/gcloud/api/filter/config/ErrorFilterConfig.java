package com.gcloud.api.filter.config;

import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.api.filter.ErrorFilter;
import com.gcloud.api.filter.FilterOrder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Configuration
@ApiRoleSelect({ApiRole.API, ApiRole.CONTROLLER_API})
public class ErrorFilterConfig {

    @Bean
    public FilterRegistrationBean errorFilterRegistration(Filter errorFilter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(errorFilter);
        registration.addUrlPatterns("*");
        registration.setOrder(FilterOrder.ERROR_FILTER);
        return registration;
    }

    @Bean
    public Filter errorFilter() {
        return new ErrorFilter();
    }

}