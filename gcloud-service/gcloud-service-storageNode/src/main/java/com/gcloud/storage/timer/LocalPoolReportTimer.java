package com.gcloud.storage.timer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.model.StoragePoolInfo;
import com.gcloud.header.storage.msg.node.pool.LocalPoolReportMsg;
import com.gcloud.header.storage.msg.node.pool.model.LocalPoolReport;
import com.gcloud.header.storage.msg.node.pool.model.LocalPoolReportResponse;
import com.gcloud.service.common.compute.uitls.VmUtil;
import com.gcloud.storage.StorageNodeProp;
import com.gcloud.storage.service.IStoragePoolService;
import com.gcloud.storage.service.impl.ReportStoragePoolModel;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class LocalPoolReportTimer {
	@Autowired
    private MessageBus bus;
	
	@Autowired
	IStoragePoolService storagePoolService;
	
	@Autowired
    private StorageNodeProp props;
	
	static SimpleDateFormat timeF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Scheduled(cron = "${gcloud.storage-node.localPool.reportCron:0 */1 * * * ?}")
    public void report(){
		String batchId= timeF.format(new Date());
		log.debug("Storage LocalPoolReportTimer begin,batchId:" + batchId);
		batchId = "1fixedBatchId";

        try {
        	String hostname= VmUtil.getHostName();
        	log.info("[ReportStoragePool]: " + props.getReportPools());
            if (props.getReportPools() != null && props.getReportPools()) {
                if (props.getPools() != null) {
                	LocalPoolReportMsg reportMsg = new LocalPoolReportMsg();
                	//暂时先固定一个批次，相当于只是定时汇报存储池信息
                	reportMsg.setCron(batchId);//timeF.format(context.getFireTime()) 
                	List<LocalPoolReport> reportPools = new ArrayList<LocalPoolReport>();
                    for (ReportStoragePoolModel pool : props.getPools()) {
                        log.info("[ReportStoragePool]: " + pool);
                        try {
                            ProviderType provider = ProviderType.get(pool.getProvider());
                            if (provider == null) {
                                throw new GCloudException("provider type " + pool.getProvider() + " not found");
                            }
                            StorageType storageType = StorageType.value(pool.getStorageType());
                            if (storageType == null) {
                                throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
                            }
                            if (provider == ProviderType.GCLOUD && storageType == StorageType.LOCAL) {
                                File storageDir = new File(pool.getPoolName());
                                if (!storageDir.exists()) {
                                    throw new GCloudException("path " + pool.getPoolName() + " not exist");
                                }else if (!storageDir.isDirectory()) {
                                    throw new GCloudException("path " + pool.getPoolName() + " is not a dir");
                                }
                                if (!pool.getPoolName().endsWith(File.separator)) {
                                    pool.setPoolName(pool.getPoolName() + File.separator);
                                }
                                
                                StoragePoolInfo info = storagePoolService.getLocalPoolInfo(pool.getPoolName());
                                if(info == null) {
                                	continue;
                                }
                                LocalPoolReport report = new LocalPoolReport();
                                report.setHostname(hostname);
                                report.setPoolName(pool.getPoolName());
                                report.setAvail(info.getAvailSize());
                                report.setTotal(info.getTotalSize());
                                report.setUsed(info.getUsedSize());
                                
                                reportPools.add(report);
                            }
                        }catch (Exception e) {
                            log.error("[ReportStoragePool] error: " + e.getMessage(), e);
                        }
                    }
                    if(reportPools.size() > 0) {
                    	log.debug(String.format("%s汇报%s个本地存储信�?", hostname, reportPools.size()));
	                    LocalPoolReportResponse res = new LocalPoolReportResponse();
	                    res.setPools(reportPools);
	                    reportMsg.setRes(res);
	                    reportMsg.setServiceId(MessageUtil.controllerServiceId());
	                    bus.send(reportMsg);
                    }
                }
            }

        } catch (Exception e) {
            log.error("LocalPoolReportTimer连接存储控制器失�?", e);
        }

        log.debug("LocalPoolReportTimer end");
		
	}
}