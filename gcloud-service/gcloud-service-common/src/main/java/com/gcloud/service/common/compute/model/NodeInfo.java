package com.gcloud.service.common.compute.model;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class NodeInfo {

	private String cpuModel;  // eg：x86_64
	private int cpus; // �?
	private int cpuFrequency; // 单位MHz
	private int cpuSockets;
	private int CoresPerSocket;
	private int ThreadsPerCore;
	private int numaCells;
	private int memorys; // KiB
	public String getCpuModel() {
		return cpuModel;
	}
	public void setCpuModel(String cpuModel) {
		this.cpuModel = cpuModel;
	}
	public int getCpus() {
		return cpus;
	}
	public void setCpus(int cpus) {
		this.cpus = cpus;
	}
	public int getCpuFrequency() {
		return cpuFrequency;
	}
	public void setCpuFrequency(int cpuFrequency) {
		this.cpuFrequency = cpuFrequency;
	}
	public int getCpuSockets() {
		return cpuSockets;
	}
	public void setCpuSockets(int cpuSockets) {
		this.cpuSockets = cpuSockets;
	}
	public int getCoresPerSocket() {
		return CoresPerSocket;
	}
	public void setCoresPerSocket(int coresPerSocket) {
		CoresPerSocket = coresPerSocket;
	}
	public int getThreadsPerCore() {
		return ThreadsPerCore;
	}
	public void setThreadsPerCore(int threadsPerCore) {
		ThreadsPerCore = threadsPerCore;
	}
	public int getNumaCells() {
		return numaCells;
	}
	public void setNumaCells(int numaCells) {
		this.numaCells = numaCells;
	}
	public int getMemorys() {
		return memorys;
	}
	public void setMemorys(int memorys) {
		this.memorys = memorys;
	}
}