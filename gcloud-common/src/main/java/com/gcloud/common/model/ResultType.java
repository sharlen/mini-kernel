package com.gcloud.common.model;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ResultType {
	Success(1, "Success", "成功"),
	Failure(2, "Failure", "失败"),
	Running(3, "Running", "进行�?"),
	Timeout(4, "Timeout", "超时");
	
	ResultType(int value, String enname, String cnname){
		this.value=(byte)value;
		this.enname=enname;
		this.cnname=cnname;
	}
	/*ResultType(byte value, String enname, String cnname){
		this.value=value;
		this.enname=enname;
		this.cnname=cnname;
	}*/
	
	private byte value;
	private String enname;
	private String cnname;
	
	public byte getValue() {
		return value;
	}
	/*public void setValue(byte value) {
		this.value = value;
	}*/
	public String getEnname() {
		return enname;
	}
	/*public void setEnname(String enname) {
		this.enname = enname;
	}*/
	public String getCnname() {
		return cnname;
	}
	/*public void setCnname(String cnname) {
		this.cnname = cnname;
	}*/
}