package com.gcloud.common.model;

import java.util.Comparator;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class IpRange {

    private long start;
    private long end;

    public IpRange(long start, long end) {
        this.start = start;
        this.end = end;
    }

    public IpRange() {
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public static class IpRangeComparator implements Comparator<IpRange> {

        @Override
        public int compare(IpRange o1, IpRange o2) {
            long result = o1.getStart() - o2.getStart();
            if(result == 0L){
                return 0;
            }else if(result > 0){
                return 1;
            }else{
                return -1;
            }
        }
    }

    public String toString(){
        return String.format("%s-%s", start, end);
    }

}