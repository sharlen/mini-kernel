package com.gcloud.boot.config;

import java.io.IOException;
import java.util.List;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class FilterCustom implements TypeFilter{
	@Override
	public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
			throws IOException {
		// TODO Auto-generated method stub
		List<String> excludePackages=ComponentPackages.getInstance().getExcludePackages();
		ClassMetadata classMetadata=metadataReader.getClassMetadata();
		//String packageName=StringUtils.substring(classMetadata.getClassName(), 0,StringUtils.lastIndexOf(classMetadata.getClassName(), "."));
		for(String module:excludePackages){
			if(classMetadata.getClassName().indexOf(module)==0){
				return true;
			}
		}
		return false;
	}

}