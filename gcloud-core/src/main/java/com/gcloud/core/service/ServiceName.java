package com.gcloud.core.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Configuration
public class ServiceName {
	@Value("${gcloud.service.api}")
	private String api;
	@Value("${gcloud.service.identity}")
	private String identity;
	@Value("${gcloud.service.controller}")
	private String controller;
	@Value("${gcloud.service.computeNode}")
	private String computeNode;
    @Value("${gcloud.service.storageNode}")
    private String storageNode;
    @Value("${gcloud.service.imageNode}")
    private String imageNode;
    @Value("${gcloud.service.imageSchedule:image-schedule}")
    private String imageSchedule;
	public String getApi() {
		return api;
	}
	public String getIdentity() {
		return identity;
	}
	public String getController() {
		return controller;
	}
	public String getComputeNode() {
		return computeNode;
	}
    public String getStorageNode() {
        return storageNode;
    }
	public String getImageNode() {
		return imageNode;
	}
	public String getImageSchedule() {
		return imageSchedule;
	}
	
}