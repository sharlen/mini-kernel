package com.gcloud.core.cache.quartz;

import java.util.Iterator;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.gcloud.core.cache.Cache;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
@QuartzTimer(fixedRate = 24 * 60 * 60 * 1000L)
public class CacheReInitQuartz extends GcloudJobBean{
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		Map<CacheType,Cache> caches= Cache.getMember();
		Iterator it=caches.values().iterator();
		while(it.hasNext()) {
			Cache c = (Cache)it.next();
			if(c.reInit()||!c.isHasInit()) {
				c.init();
			}
		}
	}

}