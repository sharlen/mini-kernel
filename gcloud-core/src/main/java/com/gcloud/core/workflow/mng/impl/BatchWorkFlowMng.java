package com.gcloud.core.workflow.mng.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.core.workflow.dao.IBatchWorkFlowDao;
import com.gcloud.core.workflow.entity.BatchWorkFlow;
import com.gcloud.core.workflow.mng.IBatchWorkFlowMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
@Transactional
public class BatchWorkFlowMng implements IBatchWorkFlowMng {
	
	@Autowired
	private IBatchWorkFlowDao batchWorkFlowDao;

	@Override
	public void update(BatchWorkFlow batchFlow, List<String> fields) {
		batchWorkFlowDao.update(batchFlow, fields);
	}

	@Override
	public void delete(BatchWorkFlow batchFlow) {
		batchWorkFlowDao.delete(batchFlow);
	}

	@Override
	public Long save(BatchWorkFlow batchFlow) {
		return batchWorkFlowDao.saveWithIncrementId(batchFlow);
	}

	@Override
	public BatchWorkFlow findById(Long id) {
		return batchWorkFlowDao.getById(id);
	}

	@Override
	public BatchWorkFlow findUnique(String field, String value) {
		return batchWorkFlowDao.findUniqueByProperty(field, value);
	}

	@Override
	public List<BatchWorkFlow> findByProperty(String field, String value) {
		return batchWorkFlowDao.findByProperty(field, value);
	}

}