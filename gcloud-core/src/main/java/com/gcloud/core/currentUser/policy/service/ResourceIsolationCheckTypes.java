package com.gcloud.core.currentUser.policy.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.core.service.SpringUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@DependsOn("springUtil")
@Component
public class ResourceIsolationCheckTypes {
	private static final Map<String, IResourceIsolationCheck> CHECKS = new HashMap<>();

    @PostConstruct
    private void init() {
        for (IResourceIsolationCheck check : SpringUtil.getBeans(IResourceIsolationCheck.class)) {
        	CHECKS.put(check.getClass().getSimpleName(), check);
        }
    }

    public static IResourceIsolationCheck get(String resourceCheckType) {
        return CHECKS.get(StringUtils.toUpperCaseFirstOne(resourceCheckType) + "ResourceIsolationCheckImpl");
    }
}