package com.gcloud.core.identity;

import com.gcloud.header.identity.api.GetApiReplyMsg;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
import com.gcloud.header.identity.role.ApiGetFunctionRightReplyMsg;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.role.model.CheckRightReplyMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class HttpIdentity implements IApiIdentity {

	@Override
	public SignUser getUserByAccessKey(String accessKey) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TokenUser checkToken(String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetUserReplyMsg getUserById(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public ApiGetRoleRightReplyMsg getRoleRight(String roleId, String regionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiDetailRoleReplyMsg getRoleDetail(String roleId) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	/*@Override
	public ApiGetFunctionRightReplyMsg getFunctionRight(String path) {
		return null;
	}*/

	@Override
	public DescribeUncheckApiReplyMsg getUncheckApi(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetLdapConfReplyMsg getLdapConf(String domainId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CheckRightReplyMsg checkRight(String roleId, String regionId, String funcPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetApiReplyMsg getApi(String path) {
		// TODO Auto-generated method stub
		return null;
	}
}