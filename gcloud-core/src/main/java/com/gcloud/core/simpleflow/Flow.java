package com.gcloud.core.simpleflow;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public abstract class Flow<T> {
	private String name;
	private boolean rollbackCurrentFlow = false;
	public Flow(){
		
	}
	public Flow(String name){
		this.name=name;
	}

	public Flow(boolean rollbackCurrentFlow) {
		this.rollbackCurrentFlow = rollbackCurrentFlow;
	}

	public Flow(String name, boolean rollbackCurrentFlow) {
		this.name = name;
		this.rollbackCurrentFlow = rollbackCurrentFlow;
	}

	public abstract void run(SimpleFlowChain chain, T data);
	public abstract void rollback(SimpleFlowChain chain,T data);
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isRollbackCurrentFlow() {
		return rollbackCurrentFlow;
	}

	public void setRollbackCurrentFlow(boolean rollbackCurrentFlow) {
		this.rollbackCurrentFlow = rollbackCurrentFlow;
	}
}