package com.gcloud.header.slb.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class VServerGroupSetType implements Serializable{
	
	@ApiModel(description = "服务器组ID")
	private String vServerGroupId;
	@ApiModel(description = "服务器组名称")
	private String vServerGroupName;
	@ApiModel(description = "服务器组协议，取值范围HTTP、TCP")
	private String vServerGroupProtocol;
	
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}
	public String getvServerGroupProtocol() {
		return vServerGroupProtocol;
	}
	public void setvServerGroupProtocol(String vServerGroupProtocol) {
		this.vServerGroupProtocol = vServerGroupProtocol;
	}
}