package com.gcloud.header.slb.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.slb.model.standard.StandardDescribeLoadBalancersResponse;
import com.gcloud.header.slb.model.standard.StandardLoadBalancerModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeLoadBalancersReplyMsg extends PageReplyMessage<StandardLoadBalancerModel>{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "负载均衡器列�?")
	private StandardDescribeLoadBalancersResponse loadBalancers;

	@Override
	public void setList(List<StandardLoadBalancerModel> list) {
		loadBalancers = new StandardDescribeLoadBalancersResponse();
		loadBalancers.setLoadBalancer(list);
	}

	public StandardDescribeLoadBalancersResponse getLoadBalancers() {
		return loadBalancers;
	}

	public void setLoadBalancers(StandardDescribeLoadBalancersResponse loadBalancers) {
		this.loadBalancers = loadBalancers;
	}
}