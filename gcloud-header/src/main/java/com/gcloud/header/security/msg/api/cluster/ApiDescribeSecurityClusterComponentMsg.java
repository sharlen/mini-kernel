package com.gcloud.header.security.msg.api.cluster;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeSecurityClusterComponentMsg extends ApiPageMessage {

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDescribeSecurityClusterComponentReplyMsg.class;
	}
	
	@ApiModel(description = "安全集群ID", require = true)
	@NotBlank
	private String id;
	
	@ApiModel(description = "组件类型")
	private String type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}