package com.gcloud.header.security.msg.api.cluster;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.security.model.DescribeSecurityClusterAddableInstanceResponse;
import com.gcloud.header.security.model.SecurityClusterInstanceType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeSecurityClusterAddableInstanceReplyMsg extends PageReplyMessage<SecurityClusterInstanceType>{

	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "可添加到安全集群的云服务器列�?")
	private DescribeSecurityClusterAddableInstanceResponse securityClusterAddableInstances;

	@Override
	public void setList(List<SecurityClusterInstanceType> list) {
		securityClusterAddableInstances = new DescribeSecurityClusterAddableInstanceResponse();
		securityClusterAddableInstances.setSecurityClusterAddableInstance(list);
	}
	
	public DescribeSecurityClusterAddableInstanceResponse getSecurityClusterAddableInstances() {
		return securityClusterAddableInstances;
	}
	
	public void setSecurityClusterAddableInstances(
			DescribeSecurityClusterAddableInstanceResponse securityClusterAddableInstances) {
		this.securityClusterAddableInstances = securityClusterAddableInstances;
	}
}