package com.gcloud.header.image.msg.api.iso;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
import com.gcloud.header.image.model.iso.DescribeIsosResponse;
import com.gcloud.header.image.model.iso.IsoType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeIsosReplyMsg extends PageReplyMessage<IsoType>{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="映像信息")
    private DescribeIsosResponse isos;
    private String regionId = ControllerProperty.REGION_ID;

    @Override
    public void setList(List<IsoType> list) {
        isos = new DescribeIsosResponse();
        isos.setIso(list);
    }

	public DescribeIsosResponse getIsos() {
		return isos;
	}

	public void setIsos(DescribeIsosResponse isos) {
		this.isos = isos;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
}