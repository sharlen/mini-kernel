package com.gcloud.header.monitor.model.standard;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardInstanceMonitorDataType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "云服务器ID")
	private String instanceId;
	@ApiModel(description = "cpu使用�?, 单位:%")
	private Integer cpu;
	@ApiModel(description = "云服务器接收到的内网数据流量, 单位:kbits")
	private Integer intranetRX;
	@ApiModel(description = "云服务器发�?�的内网数据流量, 单位:kbits")
	private Integer intranetTX;
	@ApiModel(description = "系统盘磁盘读带宽, 单位:Byte/s")
	private Integer bpsRead;
	@ApiModel(description = "系统盘磁盘写带宽, 单位:Byte/s")
	private Integer bpsWrite;
	@ApiModel(description = "查询的时间点")
	private String timeStamp;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public Integer getCpu() {
		return cpu;
	}
	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}
	public Integer getIntranetRX() {
		return intranetRX;
	}
	public void setIntranetRX(Integer intranetRX) {
		this.intranetRX = intranetRX;
	}
	public Integer getIntranetTX() {
		return intranetTX;
	}
	public void setIntranetTX(Integer intranetTX) {
		this.intranetTX = intranetTX;
	}
	public Integer getBpsRead() {
		return bpsRead;
	}
	public void setBpsRead(Integer bpsRead) {
		this.bpsRead = bpsRead;
	}
	public Integer getBpsWrite() {
		return bpsWrite;
	}
	public void setBpsWrite(Integer bpsWrite) {
		this.bpsWrite = bpsWrite;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
}