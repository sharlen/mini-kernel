package com.gcloud.header.monitor.msg.api;


import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotBlank;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeDiskMonitorDataHandlerMsg extends ApiMessage {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "磁盘ID", require = true)
	@NotBlank(message = "0060501::磁盘ID不能为空")
	private String  diskId; //磁盘id

	@ApiModel(description = "�?始时间（格式：yyyy-MM-ddTHH:mm:ssZ�?", require = true)
	@NotBlank(message = "0060502::起始时间不能为空")
	private String startTime; //起始时间，格式示例：2019-02-21 15:51:10

	@ApiModel(description = "结束时间（格式：yyyy-MM-ddTHH:mm:ssZ�?", require = true)
	@NotBlank(message = "0060503::结束时间不能为空")
	private String endTime; //结束时间，格式示例：2019-02-21 15:51:10

	@ApiModel(description = "监控数据的频率，60 �?/600�?/3600�? 默认 60 �?")
	private String period; //监控数据的频率，60 �?/600�?/3600�? 默认 60 �?
	
	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiReplyMessage.class;
	}

}