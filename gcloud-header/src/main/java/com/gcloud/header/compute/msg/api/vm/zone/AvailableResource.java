package com.gcloud.header.compute.msg.api.vm.zone;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class AvailableResource implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModel(description = "实例类型列表")
    private InstanceTypes instanceTypes;
    @ApiModel(description = "系统盘类型列�?")
    private SystemDiskCategories systemDiskCategories;
    @ApiModel(description = "数据盘类型列�?")
    private DataDiskCategories dataDiskCategories;

    public InstanceTypes getInstanceTypes() {
        return instanceTypes;
    }

    public void setInstanceTypes(InstanceTypes instanceTypes) {
        this.instanceTypes = instanceTypes;
    }

    public SystemDiskCategories getSystemDiskCategories() {
        return systemDiskCategories;
    }

    public void setSystemDiskCategories(SystemDiskCategories systemDiskCategories) {
        this.systemDiskCategories = systemDiskCategories;
    }

    public DataDiskCategories getDataDiskCategories() {
        return dataDiskCategories;
    }

    public void setDataDiskCategories(DataDiskCategories dataDiskCategories) {
        this.dataDiskCategories = dataDiskCategories;
    }

}