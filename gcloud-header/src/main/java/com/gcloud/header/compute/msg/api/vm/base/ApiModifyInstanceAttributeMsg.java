package com.gcloud.header.compute.msg.api.vm.base;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiModifyInstanceAttributeMsg extends ApiMessage{

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	@ApiModel(description="实例 ID",require=true)
	@NotBlank(message = "0010501::实例 ID不能为空")
	private String instanceId;
	@ApiModel(description="实例名称")
//	@Pattern(regexp="^[\\p{Han}A-Za-z][\\p{Han}A-Za-z0-9_] {3,19})*$",message="实例名称长度为[4,20] 英文、中文字符�?�数字或_，以大小字母或中文开�?")
	private String instanceName;
	@ApiModel(description="登录�?")
	@Length(min=3,max=20, message="0010502::登录名长度[3,20]")
	private String loginName;
	@ApiModel(description="实例密码")
	@Length(min=8,max=20, message="0010503::密码长度[8,20]")
	private String password;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
}