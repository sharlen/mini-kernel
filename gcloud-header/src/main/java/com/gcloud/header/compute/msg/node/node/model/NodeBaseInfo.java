package com.gcloud.header.compute.msg.node.node.model;

import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class NodeBaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModel(description = "节点IP")
    @TableField("node_ip")
    private String ip;
    
    @ApiModel(description = "节点主机�?")
    @TableField("hostname")
    private String hostName;
    
    @ApiModel(description = "类型")
    private String type;
    @ApiModel(description = "可用区ID")
    private String zoneId;
    @ApiModel(description = "可用区名�?")
    private String zoneName;

    @ApiModel(description = "剩余CPU�?")
    @TableField(persistence = false)
    private int cpuAvail;
    
    @ApiModel(description = "总CPU�?")
    @TableField(persistence = false)
    private int cpuTotal;
    
    @ApiModel(description = "已使用CPU�?")
    @TableField(persistence = false)
    private int cpuUsed;
    
    @ApiModel(description = "剩余内存�?, 单位MB")
    @TableField(persistence = false)
    private int memoryAvail;
    
    @ApiModel(description = "总内存量, 单位MB")
    @TableField(persistence = false)
    private int memoryTotal;
    
    @ApiModel(description = "已使用内存量, 单位MB")
    @TableField(persistence = false)
    private int memoryUsed;
    
    @ApiModel(description = "状�??, 1:正常, 0:失去连接")
    private int state;
    @ApiModel(description = "中文状�??")
    private String cnState;


	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getCnState() {
		return cnState;
	}

	public void setCnState(String cnState) {
		this.cnState = cnState;
	}

	public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public int getCpuAvail() {
        return cpuAvail < 0 ? 0 : cpuAvail;
    }

    public void setCpuAvail(int cpuAvail) {
        this.cpuAvail = cpuAvail;
    }

    public int getCpuTotal() {
        return cpuTotal;
    }

    public void setCpuTotal(int cpuTotal) {
        this.cpuTotal = cpuTotal;
    }

    public int getCpuUsed() {
        return (cpuTotal - cpuAvail) < 0 ? 0 : (cpuTotal - cpuAvail);
    }

    public void setCpuUsed(int cpuUsed) {
        this.cpuUsed = cpuUsed;
    }

    public int getMemoryAvail() {
        return memoryAvail < 0 ? 0 : memoryAvail;
    }

    public void setMemoryAvail(int memoryAvail) {
        this.memoryAvail = memoryAvail;
    }

    public int getMemoryTotal() {
        return memoryTotal;
    }

    public void setMemoryTotal(int memoryTotal) {
        this.memoryTotal = memoryTotal;
    }

    public int getMemoryUsed() {
        return memoryTotal - memoryAvail < 0 ? 0 : (memoryTotal - memoryAvail);
    }

    public void setMemoryUsed(int memoryUsed) {
        this.memoryUsed = memoryUsed;
    }


}