package com.gcloud.header.compute.msg.node.vm.adopt;

import com.gcloud.header.NodeMessage;
import com.gcloud.header.compute.msg.node.vm.model.VmDetail;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class AdoptInstanceMsg extends NodeMessage {

    private List<VmDetail> instances;
    private List<VmDetail> exceptionVms;

    public List<VmDetail> getInstances() {
        return instances;
    }

    public void setInstances(List<VmDetail> instances) {
        this.instances = instances;
    }

    public List<VmDetail> getExceptionVms() {
        return exceptionVms;
    }

    public void setExceptionVms(List<VmDetail> exceptionVms) {
        this.exceptionVms = exceptionVms;
    }
}