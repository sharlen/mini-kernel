package com.gcloud.header.compute.msg.api.vm.zone;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeZonesReplyMsg extends PageReplyMessage<AvailableZone> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "可用区列�?")
    private ApiDescribeZonesReplyWrapper zones;

    @Override
    public void setList(List<AvailableZone> list) {
        this.zones = new ApiDescribeZonesReplyWrapper();
        this.zones.setZone(list);
    }

    public ApiDescribeZonesReplyWrapper getZones() {
        return zones;
    }

    public void setZones(ApiDescribeZonesReplyWrapper zones) {
        this.zones = zones;
    }

}