package com.gcloud.header.compute.msg.node.vm.base;

import com.gcloud.header.NodeMessage;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MoveDomainMsg extends NodeMessage {
	private String instanceId;
	private String oldDoamin;
	private String oldUser;
	private String oldPassword;
	private String domain;
	private String user;
	private String password;
	public String getDomain() {
		return domain;
	}
	public String getUser() {
		return user;
	}
	public String getPassword() {
		return password;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getOldDoamin() {
		return oldDoamin;
	}
	public String getOldUser() {
		return oldUser;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldDoamin(String oldDoamin) {
		this.oldDoamin = oldDoamin;
	}
	public void setOldUser(String oldUser) {
		this.oldUser = oldUser;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
}