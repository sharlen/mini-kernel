package com.gcloud.header.compute.msg.api.model.standard;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardInstanceTypeItemType implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModel(description="实例规格的ID")
	private String instanceTypeId;
	@ApiModel(description="实例规格的ID")
	private String instanceTypeName;
	@ApiModel(description="CPU的内核数�?")
	private Integer cpuCoreCount;
	@ApiModel(description="内存大小，单位MB")
	private Double memorySize; // 单位MB
	@ApiModel(description = "是否可用")
	private boolean enabled;
	public String getInstanceTypeId() {
		return instanceTypeId;
	}
	public void setInstanceTypeId(String instanceTypeId) {
		this.instanceTypeId = instanceTypeId;
	}
	public String getInstanceTypeName() {
		return instanceTypeName;
	}
	public void setInstanceTypeName(String instanceTypeName) {
		this.instanceTypeName = instanceTypeName;
	}
	public Integer getCpuCoreCount() {
		return cpuCoreCount;
	}
	public void setCpuCoreCount(Integer cpuCoreCount) {
		this.cpuCoreCount = cpuCoreCount;
	}
	public Double getMemorySize() {
		return memorySize;
	}
	public void setMemorySize(Double memorySize) {
		this.memorySize = memorySize;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}