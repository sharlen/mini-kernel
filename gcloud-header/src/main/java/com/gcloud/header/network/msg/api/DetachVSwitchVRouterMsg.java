package com.gcloud.header.network.msg.api;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetachVSwitchVRouterMsg extends ApiMessage {
	@NotBlank(message="0020801")
	@ApiModel(description = "路由器Id", require = true)
	private String vRouterId;
	@NotBlank(message="0020802")
	@ApiModel(description = "交换机Id", require = true)
	private String vSwitchId;
	
	public String getvRouterId() {
		return vRouterId;
	}

	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

}