package com.gcloud.header.network.enums;

import java.util.Arrays;

import com.google.common.base.CaseFormat;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */





public enum RouterStatus {

	ACTIVE("�?�?"),
	DOWN("失活"),
	BUILD("已创�?"),
	ERROR("错误"),
	PENDING_CREATE("创建�?"),
	PENDING_UPDATE("删除�?"),
	PENDING_DELETE("删除�?"),
	UNRECOGNIZED("未知");

	private String cnName;

	RouterStatus(String cnName) {
		this.cnName = cnName;
	}

	public String value() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
	}

	public String getCnName() {
		return cnName;
	}
	
	public static String getCnName(String value) {
		RouterStatus enu =  Arrays.stream(RouterStatus.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
		return enu !=null?enu.getCnName():null;
	}
}