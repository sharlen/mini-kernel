package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

import java.util.List;

import javax.validation.constraints.NotBlank;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ModifySubnetAttributeMsg extends ApiMessage{
	@NotBlank(message = "0170201::子网ID不能为空")
	@ApiModel(description = "子网Id", require = true)
	private String subnetId;
	@ApiModel(description = "子网名称", require = true)
	@NotBlank(message = "0170202::子网名称不能为空")
	private String subnetName;
	@ApiModel(description = "网关IP")
	private String gatewayIp;
	@ApiModel(description = "dns服务列表")
	private List<String> dnsNameServers;
	@ApiModel(description = "是否启用dhcp,true:启用,false:禁用")
	private Boolean dhcp;


	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	public String getSubnetId() {
		return subnetId;
	}
	public void setSubnetId(String subnetId) {
		this.subnetId = subnetId;
	}
	public String getSubnetName() {
		return subnetName;
	}
	public void setSubnetName(String subnetName) {
		this.subnetName = subnetName;
	}
	public String getGatewayIp() {
		return gatewayIp;
	}
	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}
	public List<String> getDnsNameServers() {
		return dnsNameServers;
	}
	public void setDnsNameServers(List<String> dnsNameServers) {
		this.dnsNameServers = dnsNameServers;
	}
	public Boolean getDhcp() {
		return dhcp;
	}
	public void setDhcp(Boolean dhcp) {
		this.dhcp = dhcp;
	}
}