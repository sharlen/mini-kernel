package com.gcloud.header.network.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.Table;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name="gc_network")
public class VpcsItemType implements Serializable{
	
	@ApiModel(description = "区域ID")
	@TableField("region_id")
	private String regionId = ControllerProperty.REGION_ID;
	@ApiModel(description = "状�?�，active:�?�?;down:失活;build:已创�?;error:错误;pending_create:创建�?;pending_update:删除�?;pending_delete:删除�?;unrecognized:未知;")
	private String status;
	@ApiModel(description = "ID")
	@TableField("id")
	private String vpcId;
	@ApiModel(description = "vpc uuid")
	private String vpcUuid;
	@ApiModel(description = "vpc名称")
	@TableField("name")
	private String vpcName;
	@ApiModel(description = "中文状�??")
	private String cnStatus;
//	private String cidrBlock;    
	//网络的网络段
	@ApiModel(description = "子网ID列表")
	private Map<String, List> vSwitchIds;

	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	@ApiModel(description = "用户ID")
	private String userId;
	@ApiModel(description = "用户�?")
	private String userName;
	
	public String getVpcUuid() {
		return vpcUuid;
	}
	public void setVpcUuid(String vpcUuid) {
		this.vpcUuid = vpcUuid;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Map<String, List> getvSwitchIds() {
		return vSwitchIds;
	}
	public void setvSwitchIds(Map<String, List> vSwitchIds) {
		this.vSwitchIds = vSwitchIds;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}