package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeEipAddressesMsg extends ApiPageMessage {
	@ApiModel(description="弹�?�公网IP地址")
	private String eipAddress;
	@ApiModel(description="弹�?�公网IP地址申请Id")
	private String allocationId;
	
	/**
	 * EIP的状态：
		Associating：绑定中
		Unassociating：解绑中
		InUse：已分配
		Available：可�?
	 */
	@ApiModel(description="弹�?�公网IP地址状�??")
	private String status;
	
	@ApiModel(description="是否可用,true:可用;false:已用")
	private Boolean avail;
	
	@ApiModel(description = "绑定的资源ID")
	private String associateInstanceId;
	@ApiModel(description = "绑定的资源类型，取�?�范围：EcsInstance、Netcard")
	private String associateInstanceType;
	
	@Override
	public Class replyClazz() {
		return DescribeEipAddressesReplyMsg.class;
	}

	public Boolean getAvail() {
		return avail;
	}

	public void setAvail(Boolean avail) {
		this.avail = avail;
	}

	public String getEipAddress() {
		return eipAddress;
	}

	public void setEipAddress(String eipAddress) {
		this.eipAddress = eipAddress;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAssociateInstanceId() {
		return associateInstanceId;
	}

	public void setAssociateInstanceId(String associateInstanceId) {
		this.associateInstanceId = associateInstanceId;
	}

	public String getAssociateInstanceType() {
		return associateInstanceType;
	}

	public void setAssociateInstanceType(String associateInstanceType) {
		this.associateInstanceType = associateInstanceType;
	}
}