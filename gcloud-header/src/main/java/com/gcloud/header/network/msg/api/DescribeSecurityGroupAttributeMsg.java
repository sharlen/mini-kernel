package com.gcloud.header.network.msg.api;


import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotBlank;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeSecurityGroupAttributeMsg extends ApiMessage {
	
	@ApiModel(description = "安全组Id", require = true)
	@NotBlank(message = "0040701")
	private String securityGroupId;
	@ApiModel(description = "安全组方�?,egress,ingress;")
	private String direction;
	@ApiModel(description = "网络类型,IPv4,IPv6;")
	private String etherType;

	
	public String getSecurityGroupId() {
		return securityGroupId;
	}


	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public String getDirection() {
		return direction;
	}


	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getEtherType() {
		return etherType;
	}

	public void setEtherType(String etherType) {
		this.etherType = etherType;
	}

	@Override
	public Class replyClazz() {
		return DescribeSecurityGroupAttributeReplyMsg.class;
	}

}