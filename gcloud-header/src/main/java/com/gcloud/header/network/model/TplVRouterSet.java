package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class TplVRouterSet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "路由器Id")
    private String vRouterId;
    @ApiModel(description = "路由器名�?")
    private String vRouterName;
    @ApiModel(description = "网关网络")
    private String gatewayNetwork;
	public String getvRouterId() {
		return vRouterId;
	}
	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
	public String getvRouterName() {
		return vRouterName;
	}
	public void setvRouterName(String vRouterName) {
		this.vRouterName = vRouterName;
	}
	public String getGatewayNetwork() {
		return gatewayNetwork;
	}
	public void setGatewayNetwork(String gatewayNetwork) {
		this.gatewayNetwork = gatewayNetwork;
	}
}