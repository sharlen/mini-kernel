package com.gcloud.header.log.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeLogAttributesTypeResponse implements Serializable{
	@ApiModel(description = "任务列表")
	private List<LogAttributesType> log;

	public List<LogAttributesType> getLog() {
		return log;
	}

	public void setLog(List<LogAttributesType> log) {
		this.log = log;
	}

}