package com.gcloud.header.identity.role;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.role.model.FunctionRightModel;
import com.gcloud.header.identity.role.model.ResourceRight;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiSetResourceAndFuncRightMsg extends ApiMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank(message="2030501::角色ID不能为空")
	@ApiModel(description = "角色ID")
	private String roleId;
	@ApiModel(description = "资源权限列表")
    private List<ResourceRight> resourceRight;
	@ApiModel(description = "功能权限列表")
    private List<FunctionRightModel> functionRight;

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public List<ResourceRight> getResourceRight() {
		return resourceRight;
	}

	public void setResourceRight(List<ResourceRight> resourceRight) {
		this.resourceRight = resourceRight;
	}

	public List<FunctionRightModel> getFunctionRight() {
		return functionRight;
	}

	public void setFunctionRight(List<FunctionRightModel> functionRight) {
		this.functionRight = functionRight;
	}

}