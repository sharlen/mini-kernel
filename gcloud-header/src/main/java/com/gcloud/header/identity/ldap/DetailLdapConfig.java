package com.gcloud.header.identity.ldap;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailLdapConfig implements Serializable{
	@ApiModel(description ="ID")
	private String id;
	@ApiModel(description ="类型")
	private String ldapType;
	@ApiModel(description ="域名")
	private String domain;//阈名
	@ApiModel(description ="域别�?")
	private String domainAlias; //阈别�?
	@ApiModel(description ="基本DN")
	private String baseDn;//基本DN
	@ApiModel(description ="域服务器url")
	private String url;//阈服务器url
	@ApiModel(description ="管理账号")
	private String managerUser;//管理账号
	@ApiModel(description ="总共用户�?")
	private Integer totalUsers;//总共用户�?
	@ApiModel(description ="已同步用户数")
	private Integer syncUsers;//已同步用户数
	@ApiModel(description ="是否自动同步")
	private Boolean autoSync;//是否自动同步
	@ApiModel(description ="是否为默认阈")
	private Boolean defaultDomain;//是否为默认阈
	@ApiModel(description ="建时�?")
	private Date createTime; //创建时间
	@ApiModel(description ="是否可用")
	private Boolean enabled;//是否可用
	@ApiModel(description ="上一次发送时�?")
	private Date lastSendTime;//上一次发送时�?
	@ApiModel(description ="描述")
	private String description;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLdapType() {
		return ldapType;
	}
	public void setLdapType(String ldapType) {
		this.ldapType = ldapType;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDomainAlias() {
		return domainAlias;
	}
	public void setDomainAlias(String domainAlias) {
		this.domainAlias = domainAlias;
	}
	public String getBaseDn() {
		return baseDn;
	}
	public void setBaseDn(String baseDn) {
		this.baseDn = baseDn;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getManagerUser() {
		return managerUser;
	}
	public void setManagerUser(String managerUser) {
		this.managerUser = managerUser;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	public Integer getSyncUsers() {
		return syncUsers;
	}
	public void setSyncUsers(Integer syncUsers) {
		this.syncUsers = syncUsers;
	}
	public Boolean getAutoSync() {
		return autoSync;
	}
	public void setAutoSync(Boolean autoSync) {
		this.autoSync = autoSync;
	}
	public Boolean getDefaultDomain() {
		return defaultDomain;
	}
	public void setDefaultDomain(Boolean defaultDomain) {
		this.defaultDomain = defaultDomain;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Date getLastSendTime() {
		return lastSendTime;
	}
	public void setLastSendTime(Date lastSendTime) {
		this.lastSendTime = lastSendTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}