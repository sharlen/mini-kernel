package com.gcloud.header.storage.enums;

import com.google.common.base.CaseFormat;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public enum SnapshotStatus {
	AVAILABLE("可用"),
	ATTACHING("挂载�?"),
	PROGRESSING("创建�?"),
	DELETING("删除�?"),
	DOWNLOADING("下载�?"),
	UPLOADING("上传�?"),
	ERROR("错误"),
	ERROR_DELETING("删除错误"),
	ERROR_RESTORING("还原错误"),
	IN_USE("已用"),
	RESTORING("备份还原�?"),
	DETACHING("卸载�?"),
	UNRECOGNIZED("未知");

	private String cnName;

	SnapshotStatus(String cnName) {
		this.cnName = cnName;
	}

	public String value() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
	}

	public String getCnName() {
		return cnName;
	}
}