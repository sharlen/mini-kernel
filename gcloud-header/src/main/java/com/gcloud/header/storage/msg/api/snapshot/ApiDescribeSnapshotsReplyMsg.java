package com.gcloud.header.storage.msg.api.snapshot;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DescribeDisksResponse;
import com.gcloud.header.storage.model.DescribeSnapshotsResponse;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.SnapshotType;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeSnapshotsReplyMsg extends PageReplyMessage<SnapshotType> {

    private static final long serialVersionUID = 1L;
    @ApiModel(description="快照列表")
    private DescribeSnapshotsResponse snapshots;

    @Override
    public void setList(List<SnapshotType> list) {
        snapshots = new DescribeSnapshotsResponse();
        snapshots.setSnapshot(list);
    }

    public DescribeSnapshotsResponse getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(DescribeSnapshotsResponse snapshots) {
        this.snapshots = snapshots;
    }
}