package com.gcloud.header.storage.model.standard;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardSnapshotType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "快照ID")
    private String snapshotId;
	@ApiModel(description = "快照名称")
    private String snapshotName;
	@ApiModel(description = "描述")
    private String description;
	@ApiModel(description = "源磁盘ID")
    private String sourceDiskId;
	@ApiModel(description = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date creationTime;
	@ApiModel(description = "状�??")
    private String status;
	@ApiModel(description = "源磁盘大�?")
    private String sourceDiskSize;
	
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public String getSnapshotName() {
		return snapshotName;
	}
	public void setSnapshotName(String snapshotName) {
		this.snapshotName = snapshotName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSourceDiskId() {
		return sourceDiskId;
	}
	public void setSourceDiskId(String sourceDiskId) {
		this.sourceDiskId = sourceDiskId;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSourceDiskSize() {
		return sourceDiskSize;
	}
	public void setSourceDiskSize(String sourceDiskSize) {
		this.sourceDiskSize = sourceDiskSize;
	}
}