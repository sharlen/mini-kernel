package com.gcloud.header.storage.msg.api.snapshot.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiModifySnapshotAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "快照的id")
	@NotBlank(message = StorageErrorCodes.INPUT_SNAPSHOT_ID_ERROR)
    private String snapshotId;
	@ApiModel(description = "快照的显示名�?", require=true)
	@NotBlank(message = StorageErrorCodes.INPUT_SNAPSHOT_NAME_NOT_BLANK)
    @Length(max = 255, message = StorageErrorCodes.INPUT_SNAPSHOT_NAME_ERROR)
    private String snapshotName;
    
	@Override
	public Class replyClazz() {
		return StandardApiModifySnapshotAttributeReplyMsg.class;
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getSnapshotName() {
		return snapshotName;
	}

	public void setSnapshotName(String snapshotName) {
		this.snapshotName = snapshotName;
	}
}