package com.gcloud.header.storage.msg.api.pool;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiModifyStoragePoolMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiModifyStoragePoolReplyMsg.class;
    }

    @ApiModel(description = "存储池ID", require = true)
    @NotBlank(message = StorageErrorCodes.INPUT_POOL_ID_ERROR)
    private String poolId;
    @ApiModel(description = "存储池别�?", require = true)
    @NotBlank(message = StorageErrorCodes.INPUT_POOL_NAME_ERROR)
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}