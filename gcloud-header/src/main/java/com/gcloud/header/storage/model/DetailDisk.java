package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailDisk implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "磁盘ID")
	private String id;
	
	@ApiModel(description = "磁盘类型ID")
	private String category;
	
	@ApiModel(description = "磁盘名称")
	private String name;
	
	@ApiModel(description ="磁盘大小")
	private Integer size;
	
	@ApiModel(description = "磁盘类型名称")
	private String cnCategory;
	
	@ApiModel(description = "中文状�??")
	private String cnStatus;
	
	@ApiModel(description = "磁盘类型中文")
	private String cnType;
	
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	
	@ApiModel(description = "描述")
	private String description;
	
	@ApiModel(description = "")
	private String device;
	
	@ApiModel(description = "挂载实例ID")
	private String instanceId;
	
	@ApiModel(description = "挂载实例名称")
	private String instanceName;
	
	@ApiModel(description = "")
	private Boolean isTask;
	
	@ApiModel(description = "磁盘状�??,available:可用;attaching:挂载�?;creating_backup:备份�?;creating:创建�?;deleteing:删除�?;downloading:下载�?;uploading:上传�?;error:错误;error_deleteing:删除错误;error_restore:还原错误;in_use:已用;restoring_backup:备份还原�?;detaching:卸载�?;unrecognized:未知;resizing:扩展�?;")
	private String status;
	
	@ApiModel(description = "磁盘类型,system 系统�?;data 数据�?;")
	private String type;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	
	@ApiModel(description = "可用区名�?")
	private String zoneName;

	@ApiModel(description = "存储池名�?")
	private String poolName;
	
	@ApiModel(description = "存储池显示的名称")
	private String poolDisplayName;
	
	@ApiModel(description = "存储节点")
	private String poolHostName;

	@ApiModel(description = "存储类型")
	private String storageType;
	
	@ApiModel(description = "创建�?")
	private String creator;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCnCategory() {
		return cnCategory;
	}
	public void setCnCategory(String cnCategory) {
		this.cnCategory = cnCategory;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public String getCnType() {
		return cnType;
	}
	public void setCnType(String cnType) {
		this.cnType = cnType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public Boolean getIsTask() {
		return isTask;
	}
	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getPoolName() {
		return poolName;
	}
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}
	public String getPoolDisplayName() {
		return poolDisplayName;
	}
	public void setPoolDisplayName(String poolDisplayName) {
		this.poolDisplayName = poolDisplayName;
	}
	public String getPoolHostName() {
		return poolHostName;
	}
	public void setPoolHostName(String poolHostName) {
		this.poolHostName = poolHostName;
	}
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
}